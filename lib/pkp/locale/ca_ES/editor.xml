<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE locale SYSTEM "../../dtd/locale.dtd">

<!--
  * locale/ca_ES/editor.xml
  *
  * Copyright (c) 2014-2018 Simon Fraser University
  * Copyright (c) 2003-2018 John Willinsky
  * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
  * Credits: https://pkp.sfu.ca/wiki/index.php?title=OMP:_Catalan_(ca_ES)
  * Localization strings.
  -->

<locale name="ca_ES" full_name="Català">
  <message key="editor.article.confirmChangeReviewForm">Atenció: canviar el formulari de revisió afectarà totes les respostes que els revisors/es hagin fet utilitzant aquest formulari. Segur que voleu continuar?</message>
  <message key="editor.article.reviewForms">Formularis de revisió</message>
  <message key="editor.article.selectReviewForm">Seleccionar el formulari de revisió</message>
  <message key="editor.publicIdentificationNumericNotAllowed">L'identificador públic '{$publicIdentifier}' no pot ser un número.</message>
  <message key="editor.publicIdentificationPatternNotAllowed">El patró {$pattern} no està permès per a l'identificador públic.</message>
  <message key="editor.review">Revisió</message>
  <message key="editor.review.NotifyAuthorResubmit">Notifica l'autor/a la necessitat de fer revisions amb necessitat d'una altra revisió més exhaustiva:</message>
  <message key="editor.review.NotifyAuthorRevisions">Notifica l'autor/a la necessitat de fer revisions:</message>
  <message key="editor.review.anonymousReviewer">Revisor/a anònim/a</message>
  <message key="editor.review.createReviewer">Crea un revisor/a nou</message>
  <message key="editor.review.dateAccepted">Data d'acceptació de la revisió</message>
  <message key="editor.review.emailReviewer">Envia un correu electrònic al revisor/a</message>
  <message key="editor.review.enrollReviewer">Dóna d'alta un usuari/ària existent com a revisor/a</message>
  <message key="editor.submission.editorial.introduction"><![CDATA[A Editorial, el corrector/a treballa en esborranys finals abans de carregar els fitxers a <span class="pkp_help_title">Correcció</span>, on l'autor/a i d'altres els revisaran de cara a preparar i carregar una còpia de la tramesa corregida a <span class="pkp_help_title">Correcció</span>, on l'editor/a la marcarà com a <em>Aprovada</em> i l'enviarà a la fase de Producció.]]></message>
  <message key="editor.review.enrollReviewer.short">Assigna un rol a un usuari/ària existent</message>
  <message key="editor.review.errorAddingReviewer">Hi ha hagut un error en afegir un revisor/a.  Torneu-ho a intentar.</message>
  <message key="editor.review.errorDeletingReviewer">Hi ha hagut un error en eliminar un revisor/a.  Torneu-ho a intentar.</message>
  <message key="editor.review.importantDates">Dates importants</message>
  <message key="editor.review.mustSelect">Heu de seleccionar un revisor/a</message>
  <message key="editor.review.newReviewRound">Necessita una nova ronda de revisió</message>
  <message key="editor.review.noReviewFilesUploaded">Cap arxiu carregat</message>
  <message key="editor.review.noReviewFilesUploaded.details">No heu carregat cap arxiu de revisió.</message>
  <message key="editor.review.noReviews">No hi ha revisions per importar</message>
  <message key="editor.submissionLibrary.description"><![CDATA[Carregueu documents que pertanyin a la revisió i publicació d'aquesta tramesa. Les plantilles per a molts d'aquests documents les podeu trobar fent clic a <em>Veure la biblioteca de documents</em>.]]></message>
  <message key="editor.review.notInitiated">El procés de revisió encara no s'ha iniciat.</message>
  <message key="editor.review.notInitiatedCSUC">Actualment no teniu activat l’ús del RACO professional. Per poder usar aquesta funcionalitat contacteu, si us plau, amb els responsables de RACO de la vostra institució o bé a través de l’adreça electrònica raco@csuc.cat.</message>
  <message key="editor.review.personalMessageToReviewer">Correu electrònic per enviar al revisor/a</message>
  <message key="editor.review.rateReviewer">Puntuació del revisor/a</message>
  <message key="editor.review.rateReviewer.description">Avaluï la qualitat de la revisió proporcionada. Aquesta avaluació no es compartirà amb el revisor/a.</message>
  <message key="editor.review.readConfirmation">Un cop us hagueu llegit aquesta revisió, premeu «Confirma» per indicar que aquesta revisió pot continuar.</message>
  <message key="editor.review.readNewReview">Llegir la nova revisió</message>
  <message key="editor.review.readReview">Llegir la revisió</message>
  <message key="editor.review.reminder">Recordatori de revisió</message>
  <message key="editor.review.reminderError">Hi ha hagut un error en enviar un recordatori al revisor/a</message>
  <message key="editor.review.requestAccepted">Sol·licitud acceptada</message>
  <message key="editor.review.requestDeclined">Sol·licitud rebutjada</message>
  <message key="editor.review.requestDeclined.tooltip">El revisor ha declinat aquesta sol·licitud de revisió.</message>
  <message key="editor.review.requestSent">Sol·licitud enviada</message>
  <message key="editor.review.responseDue">Resposta prevista: {$date}</message>
  <message key="editor.review.revertDecision">Revertir la decisió</message>
  <message key="editor.review.reviewCompleted">S'ha completat la revisió</message>
  <message key="editor.review.reviewConfirmed">Revisió confirmada</message>
  <message key="editor.review.reviewDetails">Detalls de la revisió</message>
  <message key="editor.review.reviewDue">Revisió prevista: {$date}</message>
  <message key="editor.review.reviewDueDate">Data de venciment de la revisió</message>
  <message key="editor.review.reviewSubmitted">Revisió tramesa</message>
  <message key="editor.review.reviewerComments">Comentaris del revisor/a</message>
  <message key="editor.review.reviewerRating.none">Sense avaluar</message>
  <message key="editor.review.reviewsAdded">Revisions afegides al cos del missatge.</message>
  <message key="editor.review.sendReminder">Envia un recordatori</message>
  <message key="editor.review.skipEmail">No envieu un correu electrònic al revisor/a</message>
  <message key="editor.review.thankReviewer">Agrair al revisor/a</message>
  <message key="editor.review.thankReviewerError">Hi ha hagut un error en enviar un agraïment al revisor/a</message>
  <message key="editor.review.unassignReviewer">Desassigna un revisor/a</message>
  <message key="editor.review.unconsiderReview">Rebutjar aquesta revisió</message>
  <message key="editor.review.unconsiderReviewText">Esteu segur/a que voleu marcar aquesta revisió com a rebutjada?  Aquest historial de revisió es conservarà.</message>
  <message key="editor.review.uploadRevision">Carrega revisió</message>
  <message key="editor.review.userGroupSelect">Atorgueu un rol a l'usuari/ària amb aquest grup d'usuaris/àries revisors/ores</message>
  <message key="editor.submission.addAuditor">Assigna un auditor/a</message>
  <message key="editor.submission.editorial.copyeditingDescription"><![CDATA[Assigneu els fitxers corregits als autors/ores (i a d'altres, segons calgui) per revisar els canvis i contestar les consultes, les seves respostes de les qualsapareixen a la icona de la llibreta juntament amb els fitxers carregats. Aquests fitxers s'han marcat com a <em>Aprovats</em> pel corrector/a, el qual prepararà una còpia neta corregida per carregar a <span class="pkp_help_title">Correcció</span>, que l'editor/a comprovarà i enviarà a la fase de producció.]]></message>
  <message key="editor.submission.addReviewer">Afegeix un revisor/a</message>
  <message key="editor.submission.backToSearch">Tornar a la cerca</message>
  <message key="editor.submission.copyedit.manageCopyeditFilesDescription">Els arxius que ja s'hagin carregat a alguna fase de la tramesa es poden afegir al llistat d'arxius per corregir marcant la casella "Incloure" següent i fent clic a "Cercar": d'aquesta manera tots els arxius disponibles es llistaran i es podran triar per incloure'ls. Els arxius addicionals que encara no s'hagin carregat en cap fase es poden afegir al llistat fent clic en l'opció "Carregar arxiu".</message>
  <message key="editor.submission.copyediting.personalMessageToUser">Missatge per a l'usuari/ària</message>
  <message key="editor.submission.createNewRound">Crea una nova ronda de revisió</message>
  <message key="editor.submission.editorial.approveCopyeditDescription">El fitxer corregit ha d'aprovar-se abans d'enviar-se a la fase de producció. Voleu aprovar aquest fitxer?</message>
  <message key="editor.submission.editorial.disapproveCopyeditDescription">Si rebutjeu aquest fitxer corregit no podreu enviar-lo a la fase de producció. Voleu marcar aquest fitxer com a desaprovat?</message>
  <message key="editor.submission.externalReviewDescription">Esteu a punt d'iniciar una avaluació externa per a aquesta tramesa.  Els fitxers que formen part de la tramesa s'enumeren a sota i els podeu seleccionar per revisar-los.</message>
  <message key="editor.submission.review.reviewFilesDescription"><![CDATA[Aquests són els fitxers que es poden seleccionar per a la revisió, quan s'afegeixin revisors/ores a l'apartat <span class="pkp_help_title">Revisors/ores</span>.]]></message>
  <message key="editor.submission.fileAuditor.form.fileRequired">Heu de seleccionar almenys un fitxer per assignar a l'usuari/ària.</message>
  <message key="editor.submission.fileAuditor.form.messageRequired">Heu d'incloure-hi un missatge personal.</message>
  <message key="editor.submission.fileAuditor.form.userRequired">Heu de seleccionar un usuari a qui assignar-lo.</message>
  <message key="editor.submission.fileAuditor.skipEmail">No envieu un correu electrònic a l'auditor/a</message>
  <message key="editor.submission.fileList.includeAllStages">Inclou tots els fitxers de totes les fases accessibles del circuit de treball.</message>
  <message key="editor.submission.findAndSelectReviewer">Trobar un revisor/a</message>
  <message key="editor.submission.findAndSelectUser">Trobar un usuari/ària</message>
  <message key="editor.submission.newRound">Nova ronda de revisió</message>
  <message key="editor.submission.newRoundDescription">Esteu a punt de crear una nova ronda de revisió per aquesta tramesa.  Els fitxers que no hagin estat utilitzats a d'altres rondes de revisió s'indiquen a continuació.  Per defecte, els revisors/ores de la ronda de revisió anterior s'han seleccionat per participar en aquesta nova ronda de revisió.</message>
  <message key="editor.submission.noAuditRequested">Aquest fitxer no requereix cap auditoria</message>
  <message key="editor.submission.noReviewerFilesSelected">No hi ha fitxers seleccionats</message>
  <message key="editor.submission.noReviewerFilesSelected.details">No heu seleccionat cap fitxer perquè el revisor/a el revisi.</message>
  <message key="editor.submission.personalMessageToUser">Correu electrònic que s'enviarà a l'usuari/ària</message>
  <message key="editor.submission.production.productionReadyFiles">Fitxers preparats per a producció</message>
  <message key="editor.submission.proof.reminder">Recordatori de prova</message>
  <message key="editor.submission.proofreading.approveProof">Acceptar la prova</message>
  <message key="editor.submission.proofreading.confirmCompletion">Accepta aquesta prova per indicar que la correcció de proves s'ha completat i que l'arxiu està llest per publicar-se.</message>
  <message key="editor.submission.proofreading.confirmRemoveCompletion">Anul·la l'acceptació d'aquesta prova per indicar que la correcció de proves ja no s'ha completat i que l'arxiu no està llest per publicar-se.</message>
  <message key="editor.submission.proofreading.revokeProofApproval">Anul·lar l'acceptació de la prova</message>
  <message key="editor.submission.query.manageQueryNoteFilesDescription">Es poden adjuntar arxius per incloure'ls en aquesta discussió des de la llista següent.</message>
  <message key="editor.submission.review.currentFiles">Últim fitxer de revisions per ronda {$round}</message>
  <message key="editor.submission.review.reviewersDescription"><![CDATA[Utilitzeu <em>+Afegir revisor/a</em> per convidar lectors/ores a revisar els fitxers tramesos. La icona de la llibreta indica que s'ha enviat una revisió, que després es marca com a <em>Revisada</em> quan s'ha revisat. Quan hi hagi comentaris endarrerits, apareixerà una icona de sobres vermells com a recordatori per als revisors/ores.]]></message>
  <message key="editor.submission.revisions">Revisions</message>
  <message key="editor.submission.selectCopyedingFiles">Fitxers de correcció</message>
  <message key="editor.submission.selectFiles">Seleccionar arxius</message>
  <message key="editor.submission.selectReviewer">Selecciona un revisor/a</message>
  <message key="editor.submission.selectedReviewer">Revisor/a seleccionat</message>
  <message key="editor.submission.taskDueDate">Data de venciment de la tasca</message>
  <message key="editor.submission.taskSchedule">Planificació de la tasca</message>
  <message key="editor.submission.uploadSelectFiles">Carrega/Selecciona fitxers</message>
  <message key="editor.submissionLibrary">Biblioteca de trameses</message>
  <message key="editor.submissionReview.blind">Cegament</message>
  <message key="editor.submissionReview.doubleBlind">Doble cegament</message>
  <message key="editor.submissionReview.editReview">Editar revisió</message>
  <message key="editor.submissionReview.open">Oberts</message>
  <message key="editor.submissionReview.recordDecision">Enregistra la decisió de l'editor/a</message>
  <message key="editor.submissionReview.recordRecommendation">Enregistra la recomanació editorial</message>
  <message key="editor.submissionReview.recordRecommendation.createDiscussion">Crea una discussió de revisió sobre aquesta recomanació.</message>
  <message key="editor.submissionReview.recordRecommendation.notifyEditors">Notificar als editors/es.</message>
  <message key="editor.submissionReview.recordRecommendation.skipDiscussion">No crear una discussió de revisió.</message>
  <message key="editor.submissionReview.recordRecommendation.skipEmail">No enviar correu electrònic als editors/es.</message>
  <message key="editor.submissionReview.restrictFiles">Restringiu els fitxers disponibles</message>
  <message key="editor.submissionReview.restrictFiles.hide">Tancar la selecció d'arxiu</message>
  <message key="editor.submissionReview.reviewType">Tipus de revisió</message>
  <message key="editor.submissionReview.sendEmail">Enviar un correu electrònic de notificació a l'autor/a {$authorName}</message>
  <message key="editor.submissionReview.sendEmail.editors">Enviar un correu electrònic de notificació a l'editor/a {$editorNames}</message>
  <message key="editor.submissionReview.skipEmail">No envieu un correu electrònic a l'autor</message>
  <message key="grid.action.editQuery">Editar discussió</message>
  <message key="reviewer.list.activeReviews">{$count} actius</message>
  <message key="reviewer.list.activeReviewsDescription">Revisions actives actualment assignades</message>
  <message key="reviewer.list.averageCompletion">Mitjana de dies per a completar la revisió</message>
  <message key="reviewer.list.biography">Biografia</message>
  <message key="reviewer.list.completedReviews">Revisions completades</message>
  <message key="reviewer.list.count">{$count} revisors/es</message>
  <message key="reviewer.list.currentlyAssigned">Aquest revisor/a ja ha estat assignat per revisar aquest enviament.</message>
  <message key="reviewer.list.daySinceLastAssignment">Ahir</message>
  <message key="reviewer.list.daysSinceLastAssignment">Fa {$days} dies</message>
  <message key="reviewer.list.daysSinceLastAssignmentDescription">Dies des de l'última revisió assignada</message>
  <message key="reviewer.list.declinedReviews">Peticions de revisió rebutjades</message>
  <message key="reviewer.list.filterRating">Avaluat com a mínim</message>
  <message key="reviewer.list.itemsOfTotal">{$count} de {$total} revisors/es</message>
  <message key="reviewer.list.neverAssigned">Mai assignat</message>
  <message key="reviewer.list.reviewInterests">Interessos de revisió</message>
  <message key="reviewer.list.reviewerRating">Avaluació del revisor/a: {$rating}</message>
  <message key="reviewer.list.warnOnAssign">Aquest revisor/a està bloquejat perquè té un rol que li permet veure la identitat de l’autor/a. Per tant, la revisió cega per parells no està garantida. ¿Desitja desbloquejar-lo igualment?</message>
  <message key="reviewer.list.warnOnAssignUnlock">Desbloquejar</message>
  <message key="submission.queries.attachedFiles">Arxius adjunts</message>
  <message key="submission.queries.messageRequired">Es necessita un missatge de discussió.</message>
  <message key="submission.queries.subjectRequired">Es necessita un tema de discussió.</message>
</locale>
