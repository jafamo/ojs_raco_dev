{**
 * templates/dashboard/mysubmission.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Dashboard mysubmission.
 *}
{include file="common/header.tpl" pageTitle="navigation.submissions"}


<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
	// Attach the JS file tab handler.
	$(function() {ldelim}
		$('#dashboardTabs').pkpHandler('$.pkp.controllers.TabHandler');
        //hide button search
        $(".pkpListPanel__header").hide();
	{rdelim});
</script>
	
<div id="dashboardTabs" class="pkp_controllers_tab">

	
	{if array_intersect(array(ROLE_ID_SITE_ADMIN, ROLE_ID_MANAGER), (array)$userRoles)}

    <div style="margin-left: 30px;">
		<h1>{translate key="common.queue.long.submissionsUnassigned"}</h1>
		
			<div id="unassigned">
				{help file="submissions.md" section="unassigned" }
				<div class="pkp_content_panel">
					{assign var="uuid" value=""|uniqid|escape}
					<div id="unassigned-list-handler-{$uuid}">
						<script type="text/javascript">
							pkp.registry.init('unassigned-list-handler-{$uuid}', 'SubmissionsListPanel', {$unassignedListData});
						</script>
					</div>
				</div>
			</div>
			<h1>{translate key="common.queue.long.active"}</h1>
			<div id="active">
				{help file="submissions.md" section="active" }
				<div class="pkp_content_panel">
					{assign var="uuid" value=""|uniqid|escape}
					<div id="active-list-handler-{$uuid}">
						<script type="text/javascript">
							pkp.registry.init('active-list-handler-{$uuid}', 'SubmissionsListPanel', {$activeListData});
						</script>
					</div>
				</div>
			</div>
		{/if}

		<h1>{translate key="navigation.archives"}</h1>
		<div id="archived">
			{help file="submissions.md" section="archives" }
			<div class="pkp_content_panel">
				{assign var="uuid" value=""|uniqid|escape}
				<div id="archived-list-handler-{$uuid}">
					<script type="text/javascript">
						pkp.registry.init('archived-list-handler-{$uuid}', 'SubmissionsListPanel', {$archivedListData});
					</script>
				</div>
			</div>
		</div>
	</div>

</div>
{*/if*}

{include file="common/footer.tpl"}
