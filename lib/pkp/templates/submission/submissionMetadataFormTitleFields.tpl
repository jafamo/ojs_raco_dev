{**
 * templates/submission/submissionMetadataFormTitleFields.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Submission's metadata form title fields. To be included in any form that wants to handle
 * submission metadata.
 *}

<div class="pkp_helpers_clear">
	{fbvFormSection for="title" title="common.prefix" inline="true" size=$fbvStyles.size.SMALL}
		{fbvElement label="common.prefixAndTitle.tip" type="text" multilingual=true name="prefix" id="prefix" value=$prefix readonly=$readOnly maxlength="32"}
	{/fbvFormSection}
	{fbvFormSection for="title" title="common.title" inline="true" size=$fbvStyles.size.LARGE required=true}
		{fbvElement type="text" multilingual=true name="title" id="title" value=$title readonly=$readOnly maxlength="255" required=true}
	{/fbvFormSection}
</div>
{fbvFormSection title="common.subtitle" for="subtitle"}
	{fbvElement type="text" multilingual=true name="subtitle" id="subtitle" value=$subtitle readonly=$readOnly}
{/fbvFormSection}



{fbvFormSection title="common.abstract" for="abstract" required=$abstractsRequired}
	{if $wordCount}
		<p class="pkp_help">{translate key="submission.abstract.wordCount.description" wordCount=$wordCount}
	{/if}
	{fbvElement type="textarea" multilingual=true name="abstract" id="abstract" value=$abstract rich="extended" readonly=$readOnly wordCount=$wordCount}

{/fbvFormSection}


{*
Template on surt el select  a la part de Metadata 

	{fbvFormSection title="languageCsuc" for="languageCsuc" size=$smarty.const.MEDIUM }
		{php}                    
			$this->assign("miarray", array( 
				und => "Altres", 
				cat => "Català", 
				spa => "Castellà", 
				eng => "Anglès",
				fra => "Francès", 
				mul => "Més llengues", 
				por => "Portuguès", 
				ita => "Italià",
				deu => "Alemany", 
				eus => "Basc", 
				gig => "Gallec"							
			));
			$this->assign("languageCsuc", $languageCsuc);
		{/php}

		{fbvElement label="submission.submit.submissionLocaleDescription"  type="select" id="languageCsuc" from=$myOptions selected=$languageCsuc translate=false  size=$fbvStyles.size.MEDIUM}
		
	{/fbvFormSection}
                  <br><br><br>  

*}				  
