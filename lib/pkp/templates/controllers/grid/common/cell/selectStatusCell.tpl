{**
 * templates/controllers/grid/common/cell/selectStatusCell.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2000-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * a regular grid cell (with or without actions)
 *}
{assign var=cellId value="cell-"|concat:$id|uniqid}
<span id="{$cellId}">
	{if count($actions) gt 0}
		{assign var=defaultCellAction value=$actions[0]}
		{include file="linkAction/buttonGenericLinkAction.tpl" action=$defaultCellAction buttonSelector="#select-"|concat:$cellId}
	{/if}


<input type="checkbox" id="select-{$cellId}"


{if strpos($cellId,'ca_ES-ui')!= false }
	
	{if $name}name="{$name|escape}"{/if} 
	{if $value}value="{$value|escape}"{/if} style="height: 15px; width: 15px;" 
	{if $selected}checked="checked" disabled="disabled"{/if} 
	
	 


<!--show only ES, EN -->
{elseif strpos($cellId,'es_ES-uiLocale')!= false || strpos($cellId,'en_US-uiLocale')!= false }
	{if $name}name="{$name|escape}"{/if} 
	{if $value}value="{$value|escape}"{/if} style="height: 15px; width: 15px;" 	
	{if $selected}checked="checked"{/if} {if $disabled}disabled="disabled"{/if}/>	


{elseif  strpos($cellId,'formLocale')!=false || strpos($cellId,'submissionLocale')!=false}
	{if $name}name="{$name|escape}"{/if} 
	{if $value}value="{$value|escape}"{/if} style="height: 15px; width: 15px;" 
	{if $selected}checked="checked"{/if} {if $disabled}disabled="disabled"{/if}/>	

{elseif strpos($cellId,'de_DE-uiLocale')!= false || strpos($cellId,'fr_CA-uiLocale')!= false || strpos($cellId,'it_IT-uiLocale')!= false ||
		strpos($cellId,'pt_BR-uiLocale')!= false || strpos($cellId,'pt_PT-uiLocale')!= false || strpos($cellId,'zh_TW-uiLocale')!= false ||
		strpos($cellId,'eu_ES-uiLocale')!= false || strpos($cellId,'an_ES-uiLocale')!= false || strpos($cellId,'bb_ES-uiLocale')!= false ||
		strpos($cellId,'gl_ES-uiLocale')!= false || strpos($cellId,'oc_ES-uiLocale')!= false || strpos($cellId,'zh_CN-uiLocale')!= false ||
		strpos($cellId,'ru_RU-uiLocale')!= false }	 
{if $name}name="{$name|escape}"{/if} 
	{if $value}value="{$value|escape}"{/if} style="height: 15px; width: 15px;" disabled="disabled"
	 

{else}
 	{if $name}name="{$name|escape}"{/if} 
	{if $value}value="{$value|escape}"{/if} style="height: 15px; width: 15px;" 
	{if $selected}checked="checked"{/if} {if $disabled}disabled="disabled"{/if}/>
	
{/if}


</span>

