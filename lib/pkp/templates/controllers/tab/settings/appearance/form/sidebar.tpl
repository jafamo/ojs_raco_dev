{**
 * controllers/tab/settings/appearance/form/sidebar.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Form fields for configuring the sidebars
 *
 *}
{if $isSiteSidebar}
    {assign var=component value="listbuilder.admin.siteSetup.AdminBlockPluginsListbuilderHandler"}    
{else}
    {assign var=component value="listbuilder.settings.BlockPluginsListbuilderHandler"}   
{/if}

{url|assign:blockPluginsUrl router=$smarty.const.ROUTE_COMPONENT component=$component op="fetch" escape=false}

{load_url_in_div id="blockPluginsContainer" url=$blockPluginsUrl}


{php}
    //Obtindre els plugins de tipus 'blocks'
	$blockPlugins = PluginRegistry::getPlugins('blocks');
    
	
    echo "<label> Plugins </label><br>";
	//definim en array per mostrar ordenar i mostrar després  
    $arr = array();
    //$i=1;

    
    
    foreach ($blockPlugins as $blockPlugin) {        
        

        if (strpos(strtolower($blockPlugin->getDisplayName()), "publisher") !== false) {              
            $blockPlugin->setSeq(3);
            $blockPlugin->setBlockContext(1);
            $blockPlugin->setEnabled(1);
            
        }
        if (strpos(strtolower($blockPlugin->getDisplayName()), 'language') !== false) {            
            $blockPlugin->setSeq(100);
            $blockPlugin->setBlockContext(0);
            $blockPlugin->setEnabled(0);
            
        }
        if (strpos(strtolower($blockPlugin->getDisplayName()), 'participa') !== false) {
            $blockPlugin->setSeq(2);
            $blockPlugin->setBlockContext(1);
            $blockPlugin->setEnabled(1);
            
        }
        
        if (strpos(strtolower($blockPlugin->getDisplayName()), "certificado") !== false) {
            //$blockPlugin->setSeq(4);
            //$blockPlugin->setBlockContext(1);
            //$blockPlugin->setEnabled(1);
        }
        
        if (strpos(strtolower($blockPlugin->getDisplayName()), "keyword") !== false) {
            $blockPlugin->setSeq(5);
            $blockPlugin->setBlockContext(1);
            $blockPlugin->setEnabled(1);
        }

        if (strpos(strtolower($blockPlugin->getDisplayName()), "submission") !== false) {
            $blockPlugin->setSeq(10);
            $blockPlugin->setBlockContext(1);
            $blockPlugin->setEnabled(1);
        }

        $arr[$blockPlugin->getSeq()]=$blockPlugin->getDisplayName();
        
        
        //
		//echo "<input type='radio' checked disabled>"."  ".$blockPlugin->getDisplayName()." __ ".$blockPlugin->getSeq(). "<br>";
        //echo "<input type='radio' checked disabled>"."  ".$blockPlugin->getDisplayName()." __ ".$blockPlugin->getSeq(). "<br>";
		//echo "<br>";
       
	}
    
    ksort($arr);
    foreach($arr as $k => $v){        
        echo "<input type='radio' checked disabled>"."  ".$k." ".$v. "<br>";
        //echo $k." ".$v."<br>";
    }
    echo "<br><br>";
    
{/php}
