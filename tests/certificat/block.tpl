{**
 * plugins/blocks/certificat/block.tpl
 *
 * Copyright (c) 2019  CSUC
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- "Certificat" block.
 *}
<div class="pkp_block block_developed_by">
	<div class="content">
            
            <h1>Plugins:</h1>
		<a href="http://pkp.sfu.ca/ojs/">
			{translate key="common.openJournalSystems"}
		</a>
       <div class="pkp_block">
          
           <!-- #TODO   Mostrar Carhus si te una A,B,C o D -->
           <span class="title">carhus</span>
            <p>Nivell: {$currentJournal->getSetting('carhus')}</p>

            {if $currentJournal->getSetting('fecyt') != 0}
                <h1>{$smarty.const}
                 <a href="{$baseUrl}/index.php/index/fecyt">                
                 <p>fecyt</p></a>
            {$currentJournal->getSetting('fecyt')}
            {/if}
            
            {if $currentJournal->getSetting('jcr') != 0}
                <span class="title">jcr</h2></span>
               {$currentJournal->getSetting('jcr')}
            {/if}
        </div>
                
	</div>
</div>
