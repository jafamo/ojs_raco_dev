<?php

/**
 * @defgroup plugins_blocks_certificat Developed By CSUC
 */

/**
 * @file plugins/blocks/certificat/index.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_blocks_certificat
 * @brief Wrapper for "certificat" block plugin.
 *
 */

require_once('CertificatBlockPlugin.inc.php');

return new CertificatBlockPlugin();

?>
