{**
 * templates/frontend/pages/estadistiques.tpl
 *
 * Copyright (c) 2014-2018 Simon Fraser University
 * Copyright (c) 2000-2018 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Generic error page.
 * Displays a simple error message and (optionally) a return link.
 *}


 {include file="frontend/components/header.tpl"}

{literal}

<style>
.navlist li {
  display: inline;
  
}
.navlist{
  margin-left:-50px;
}
.separacio{
  padding-right:25px;
}
li a:hover {
  background-color: #A80724;
}
</style>

<script>

function myFunction(path) {
  $("#imagePreview").load(path);
 // alert(path);

}
{/literal}

</script>



<div id="main-content" class="page ">
	{include file="frontend/components/breadcrumbs.tpl" currentTitleKey="estadistiques.titol"}
</div>

<div class="page_header">
    <h1>{translate key="estadistiques.titol"}</h1>
</div>
<br>

<div class="row " style="margin-right:0px !important">


  <div class="navListItems" style="padding-bottom:40px;margin-left:-25px;">
      <ul class="navbar-nav navbar-expand-lg navbar-light">
        <a class="separacio" href="#{translate key="estadistiques.mesos"}">{translate key="estadistiques.mesos"}</a>
        <a class="separacio" href="#{translate key="estadistiques.anys"}">{translate key="estadistiques.anys"}</a>
        <a class="separacio" href="#{translate key="estadistiques.paisos"}">{translate key="estadistiques.paisos"}</a>
        <a class="separacio" href="#{translate key="estadistiques.consultats"}">{translate key="estadistiques.consultats"}</a>
      </ul>
  </div>
</div>
 <br><br>
<div class="container" >
  <div class="row">
    
    <!-- comprovar el settings de les estadistiques -->
      <div class="col">
        <h3 id="{translate key="estadistiques.mesos"}">{translate key="estadistiques.mesos"}</h3>
        <img src="{$site}/img/statistics/journals/{$journal_id}/consultes13m_{$journal_id}_{$currentLocale}.png"} />          
      </div>

      <div class="col">       
        <h3 id="{translate key="estadistiques.anys"}">{translate key="estadistiques.anys"}</h3>      
        <img src="{$site}/img/statistics/journals/{$journal_id}/consultesAcumu5y_{$journal_id}_{$currentLocale}.png"} /> 
      </div>


      <div class="col">
        <h3 id="{translate key="estadistiques.paisos"}">{translate key="estadistiques.paisos"}</h3>
        <img src="{$site}/img/statistics/journals/{$journal_id}/paisos10_{$journal_id}_{$currentLocale}.png"} /> 
      </div>

      <div class="col">
      <h3 id="{translate key="estadistiques.consultats"}">{translate key="estadistiques.consultats"}</h3><br>
          <ul class="navlist">  
            {foreach from=$anios item=item }                                   		            
                <li style="padding-left:10px"><a  onclick="myFunction('{$baseUrl}/templates/statistics/journals/top30/llistaTop30ByJournal_{$item}_{$journal_id}_{$currentLocale}.html')">{$item}</a>
                </li>                                 	
            {/foreach} 
          <ul>               
        <br>
      </div>
  </div>
<div >

        
  <div id="imagePreview" style="margin-left:-2px; margin-right:300px;">
  </div>

</div>
</div>



{include file="frontend/components/footer.tpl"}
