{**
 * templates/frontend/components/languageSwitcher.tpl
 *
 * Copyright (c) 2014-2016 Simon Fraser University
 * Copyright (c) 2003-2016 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief A re-usable template for displaying a language switcher dropdown.
 *
 * @uses $currentLocale string Locale key for the locale being displayed
 * @uses $languageToggleLocales array All supported locales
 * @uses $id string A unique ID for this language toggle
 *}

{if $languageToggleLocales && $languageToggleLocales|@count > 1}
	<div id="{$id|escape}" class="dropdown pull-right language-toggle">
	<button class="btn btn-default dropdown-toggle "  style="padding-top: 10px; border-color: #fff"  type="button" id="languageToggleMenu{$id|escape}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
		<!-- ORIGINAL <button class="btn dropdown-toggle col-xs-12" type="button" id="languageToggleMenu{$id|escape}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
			<span class="sr-only">{translate key="plugins.themes.healthSciences.language.toggle"}</span>
			{$languageToggleLocales[$currentLocale]}
		</button>
		<div class="dropdown-menu col-xs-12" aria-labelledby="languageToggleMenu{$id|escape}">
			{foreach from=$languageToggleLocales item="localeName" key="localeKey"}
				{if $localeKey !== $currentLocale}
				<ul>					
					<a class="" aria-haspopup="true" href="{url router=$smarty.const.ROUTE_PAGE page="user" op="setLocale" path=$localeKey source=$smarty.server.REQUEST_URI}">
						{$localeName}<br>
					</a>
					
				</ul>
				{/if}
			{/foreach}
		</div>
	</div>
{/if}
