
<h2>{translate key="user.register.form.NoteLegal"}</h2>
<br>
<p>{translate key="user.register.form.NoteLegalLabel"}</p>
<br>


<style type="text/css">
    {literal}

        table.full {
            border-collapse: separate;
            border-spacing: 2px;
            width: 100%;
        }

        th, td {
            
            padding: .5em;
        }
        tr {
            display: table-row;
            vertical-align: inherit;
            border-color: inherit;
        }

        table.full th, table.full td {
            border: 1px;    
        }

        table.full {
            border-collapse: separate;
            border-spacing: 2px;
            width: 100%;
        }
    {/literal}
</style>

<table style="background-color: #E9F1F3;" align="left" border="1" cellpadding="1" cellspacing="1" class="full">
    <tbody>
        <tr>
            <td>{translate key="user.register.form.NoteLegalResponsable"}</td>
            <td>{translate key="user.register.form.NoteLegalContacteCsuc"}</td>
        </tr>
        <tr>
            <td>{translate key="user.register.form.NoteLegalContacteDesc"}</td>
            <td>{translate key="user.register.form.NoteLegalContacteCsuc"}</td>
        </tr>
        <tr>
            <td>{translate key="user.register.form.NoteLegalFinalitat"}</td>
            <td>{translate key="user.register.form.NoteLegalFinalitatTractament"}</td>
        </tr>
        <tr>
            <td>{translate key="user.register.form.NoteLegalLegitimacio"}</td>
            <td>{translate key="user.register.form.NoteLegalConsentiment"}</td>
        </tr>
        <tr>
            <td>{translate key="user.register.form.NoteLegalContacteCsucDestinataris"}</td>
            <td>{translate key="user.register.form.NoteLegalFestinatarisTercers"}</td>
        </tr>
        <tr>
            <td>{translate key="user.register.form.NoteLegalContacteCsucDrets"}</td>
            <td>{translate key="user.register.form.NoteLegalContacteCsucDretsPersones"}</td>
        </tr>
        <tr>
            <td>{translate key="user.register.form.NoteLegalContacteCsucTermini"}</td>
            <td>{translate key="user.register.form.NoteLegalContacteCsucTerminiVigencia"}</td>
        </tr>
        <tr>
            <td>{translate key="user.register.form.NoteLegalContacteCsucReclamacio"}</td>
            <td>{translate key="user.register.form.NoteLegalContacteCsucReclamacioCsuc"}</td>
        </tr>
    </tbody>
</table>
