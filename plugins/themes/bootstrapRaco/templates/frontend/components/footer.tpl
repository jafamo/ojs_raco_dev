{**
 * templates/frontend/components/footer.tpl
 *
 * Copyright (c) 2014-2017 Simon Fraser University Library
 * Copyright (c) 2003-2017 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Common site frontend footer.
 *
 * @uses $isFullWidth bool Should this page be displayed without sidebars? This
 *       represents a page-level override, and doesn't indicate whether or not
 *       sidebars have been configured for thesite.
 *}


<style type="text/css">
{literal}
/* this is an intersting idea for this section */
.footerCsuc{
	padding-top: 5rem; 
	/*
	margin-left: -225px; 
	margin-right: -222px;
	*/
}
.colorFooter{
	 background-color:#EFEFEF;
}

.titolFooter{
	font-size: 16px;
	font-weight: bold;
	font-family: Graphik, -apple-system, BlinkMacSystemFont, Segoe UI, Helvetica, Arial, sans-serif;
}

.rowCsuc{
	padding-left: 213px;
	padding-right: 200px;
	padding-top: 15px;
	margin-left: 0px !important;
	margin-right: 0px !important;
}
/*
.rowCsuc{
	padding-left: 150px;
}*/

.linkCsuc{
	color:#565650 !important;
	margin-top:5px;
}

.imagenCsuc{
	height: auto;
    max-width: 100%;
}

.footerCsuc ul li {
	margin-top:5px;
}

.footerCsuc ul{
	padding-left:20px;
}

{/literal}
</style>
	</main>

	{* Sidebars *}
	{if empty($isFullWidth)}
		{capture assign="sidebarCode"}{call_hook name="Templates::Common::Sidebar"}{/capture}
		{if $sidebarCode}
			<aside id="sidebar" class="pkp_structure_sidebar left col-xs-12 col-sm-2 col-md-4" role="complementary" aria-label="{translate|escape key="common.navigation.sidebar"}">
				{$sidebarCode}
			</aside><!-- pkp_sidebar.left -->
		{/if}
	{/if}
	</div><!-- pkp_structure_content -->

	<footer class="footer"  style="background-color:#FFFFFF; width:100% !important">

		<div class="" style="width:100% !important;">

			</div> <!-- .row -->

<!-- CSUC -->			
	
<div class="footerCsuc" style="padding-top: 5rem;">
			<div style=" background-color:#EFEFEF;">
				<div class="row rowCsuc">

{translate key="plugins.themes.healthSciences.footer" base_url=$base_url}
				</div><!--.row-->
			</div>
		</div>


	
		</div><!-- .container -->
	</footer>
</div><!-- pkp_structure_page -->

{load_script context="frontend" scripts=$scripts}

{call_hook name="Templates::Common::Footer::PageFooter"}
</body>
</html>

