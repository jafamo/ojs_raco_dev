{**
 * templates/admin/journalSettings.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Basic journal settings under site administration.
 *
 *}

<script>
    $(function() {ldelim}
        // Attach the form handler.
        $('#journalSettingsForm').pkpHandler('$.pkp.controllers.form.AjaxFormHandler');
        {rdelim});
</script>

<form class="pkp_form" id="journalSettingsForm" method="post" action="{url router=$smarty.const.ROUTE_COMPONENT component="grid.admin.journal.JournalRacoGridHandler" op="updateContext"}">
    {csrf}
    {include file="controllers/notification/inPlaceNotification.tpl" notificationId="journalSettingsNotification"}

    {if $contextId}
        {fbvElement id="contextId" type="hidden" name="contextId" value=$contextId}
    {else}
        <p>{translate key="admin.journals.createInstructions"}</p>
    {/if}

    {fbvFormArea id="journalSettings"}
    {fbvFormSection title="manager.setup.journalTitle" required=true for="name"}
    {fbvElement type="text" id="name" value=$name multilingual=true required=true}
    {/fbvFormSection}
    {fbvFormSection title="admin.journals.journalDescription" for="description"}
    {fbvElement type="textarea" id="description" value=$description multilingual=true rich=true}
    {/fbvFormSection}
    {fbvFormSection title="journal.path" required=true for="path"}
    {fbvElement type="text" id="path" value=$path size=$smarty.const.SMALL maxlength="32" required=true}
    {url|assign:"sampleUrl" router=$smarty.const.ROUTE_PAGE journal="path"}
        {** FIXME: is this class instruct still the right one? **}
        <span class="instruct">{translate key="admin.journals.urlWillBe" sampleUrl=$sampleUrl}</span>
    {/fbvFormSection}

    {fbvFormSection for="enabled" list=true}
    {if $enabled}{assign var="enabled" value="checked"}{/if}
    {fbvElement type="checkbox" id="enabled" checked=$enabled value="1" label="admin.journals.enableJournalInstructions"}
    {/fbvFormSection}

        {** CUSTOM CSUC **}


        {** SEGELLS **} {** Recoger de la base de datos**}

    {fbvFormSection for="enableFecyt" list="true"  title="admin.Journals.fecyt"}
    {if $enableFecyt} {assign var="enableFecyt" value="checked"} {/if}
    {fbvElement type="checkbox" id="enableFecyt" label="FECYT"  value="1" checked=$enableFecyt|compare:true}
    {/fbvFormSection}


    {fbvFormSection for="enableJcr" list=true title="admin.Journals.jcr"}
    {if $enableJcr}{assign var="enableJcr" value="checked"}{/if}
    {fbvElement type="checkbox" id="enableJcr" value="1" checked=$enableJcr|compare:true label="JCR"}
    {/fbvFormSection}


    {fbvFormSection title="admin.Journals.carhus" for="enableCarhus" size=$smarty.const.SMALL }
    {php}
        /* Array con las opciones de Carhus*/
        //$miarray = array(" ","A", "B", "C", "D");
        $this->assign("miarray", array( 0=>' ', 1=>'A', 2=>'B', 3=>'C', 4=>'D'));
        $this->assign('miseleccion', $enableCarhus);
    {/php}

        <select name="enableCarhus" title="admin.Journals.carhus">
            {html_options  output=$miarray values=$miarray selected=$enableCarhus}
        </select>
    {/fbvFormSection}

{fbvFormSection title="Raco Profesional" for="enableProfessional" size=$smarty.const.SMALL }
    {php}
        /* Array con las opciones Professional*/
        //$miarray = array(" ","Basic", "Professional");
        $this->assign("miarray", array( 0=>' ', 1=>'Basic', 3=>'Professional'));
        $this->assign('miseleccion', $enableProfessional);
    {/php}

        <select name="enableProfessioanl" title="enableProfessional">
            {html_options  output=$miarray values=$miarray selected=$enableProfessional}
        </select>
    {/fbvFormSection}


<!-- PRO con valor boolean
    {fbvFormSection for="enableProfessional" list=true}
        {if $enableProfessional}{assign var="enableProfessional" value=checked}{/if}
        {fbvElement type="checkbox" id="enableProfessional" value="1" checked=$enableProfessional|compare:true  label="RACO PROFESSIONAL"}
    {/fbvFormSection}
-->

        {** Participants **}

        <h2>Publishers</h2>

        {*$publishers|@print_r*}
        <!--
                 <select name="publishers" size="10" style="width: 550px;height: 250px" multiple >

                   {* html_options options=$publishers selected=""*}
                </select>
                !-->
        {** Aço funciona, pero no guarda a la BD
                       {fbvFormSection title="publishers" for="publishers"}
                            <select name="publishers" size="10" style="width: 650px;height: 250px" multiple>
                               <option value=" " style="background-color: #ffff">CAP OPCIO</option>
                                {foreach from=$mypublishers key=key item=item }
                                    {html_options output=$item values=$key selected=$publishers}
                               {/foreach}
                            </select>

                      {/fbvFormSection}
        **}

        <h2>els meus participants</h2>

    {fbvFormSection title="participants" description="manager.statistics.reports.objectType" for="mypublishers" style="width: 650px;height: 250px"}
        <option value=" " style="background-color: #fbb450">CAP Participant</option>
    {fbvElement type="select" name="participants" id="particpants" from=$mypublishers multiple="multiple" selected=$participants translate=false style="width: 650px;height: 250px"}
    {/fbvFormSection}


        {**
               {fbvFormSection title="materias" for="subjects"}
                   <h2>Materies</h2>
                   {if $subjects}{assign var="subjects" value=selected}{/if}
               <select name="subjects" size="10" style="width: 650px;height: 250px" multiple>
                  <option value=" " style="background-color: #ffff">CAP MATERIA</option>
                   {foreach from=$materias key=key item=item }
                       <option value="{$item.code}" selected="{$item.code}">{$item.code} - {$item.title_lang}</option>

                  {/foreach}
               </select>
             {/fbvFormSection}
        **}


{* Es una lista y no se acaba de

    {fbvFormSection title="subjects"  for="subjects" style="width: 650px;height: 250px"}
        <option value=" " style="background-color: #fbb450">CAP Materia</option>
        {fbvElement type="select" name="subjects" id="subjects" from=$materias multiple="multiple" selected=$materias translate=false }
    {/fbvFormSection}

*}
{*
        <h1>SUPER MATERIAS</h1>
    {fbvFormSection title="materias" for="subjects" }
        <select name="subjects" title="subjects" multiple style="width: 650px;height: 250px" >
            <option value=" " style="background-color: #ffff">CAP MATERIA</option>
            {foreach from=$materias key=key item=item }
                <option value="{$item.code}" selected="$item">{$item.code} - {$item.title_lang}</option>
            {/foreach}
        </select>
    {/fbvFormSection}
*}

    {fbvFormSection  for="subjects"}
                            <select name="subjects" size="10" style="width: 650px;height: 250px" multiple>
                               <option value=" " style="background-color: #ffff"; width: 630px;>CAP OPCIO</option>
                                {foreach from=$materias key=key item=item }
                                     <option value="{$item.code}" selected="asd ">{$item.code} - {$item.title_lang}</option>
                               {/foreach}
                            </select>

                      {/fbvFormSection}
        

    {*$materias[2].title_lang|@var_dump*}

        {** Test
         {foreach name=outer item=item from=$materias}
             {$item.code}:{$item.title_lang}<br />
         {/foreach}
         **}

        <!--<select name="materias" size="10" style="width: 650px;height: 250px" multiple>
          <option value=" " style="background-color: #ffff">CAP OPCIO</option> -->
        <!--</select>     -->



        <p><span class="formRequired">{translate key="common.requiredField"}</span></p>
    {fbvFormButtons id="journalSettingsFormSubmit" submitText="common.save"}
    {/fbvFormArea}
</form>
