<?php

/**
 * @defgroup plugins_themes_bootstrapRaco Theme plugin for base Bootstrap 3 Raco theme
 */

/**
 * @file plugins/themes/default/index.php
 *
 * Copyright (c) 2014-2017 Simon Fraser University Library
 * Copyright (c) 2003-2017 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_themes_default
 * @brief Wrapper for default theme plugin.
 *
 */

require_once('BootstrapRacoThemePlugin.inc.php');

return new BootstrapRacoThemePlugin();

?>
