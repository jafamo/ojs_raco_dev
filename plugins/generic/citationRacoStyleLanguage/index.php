<?php
/**
 * @defgroup plugins_generic_citationRacoStyleLanguage
 */
/**
 * @file plugins/generic/citationRacoStyleLanguage/index.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_generic_citationRacoStyleLanguage
 * @brief Wrapper for Citation Raco Style Language plugin.
 *
 */
require_once('CitationRacoStyleLanguagePlugin.inc.php');

return new CitationRacoStyleLanguagePlugin();

?>
