{**
 * plugins/blocks/publisher/block.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- "Developed By" block.
 *}
<div class="pkp_block ">
	<div class="content">		
		<div style="text-align:center">
		
		{foreach from=$publishers item=item key=key name=name}		
   			<a href="{$item.url}"> 
        	<img  src="{$baseUrl}/img/publishers/{$item.logo}"  />
    		</a>   
			<br>    
		{/foreach}
		
  		</div>
	</div>
</div>
