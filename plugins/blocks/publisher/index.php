<?php

/**
 * @defgroup plugins_blocks_developedBy Developed By block plugin
 */

/**
 * @file plugins/blocks/publisher/index.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_blocks_developedBy
 * @brief Wrapper for "developed by" block plugin.
 *
 */

require_once('PublisherBlockPlugin.inc.php');

return new PublisherBlockPlugin();

?>
