<?php

/**
 * @defgroup plugins_blocks_participant Participant block plugin
 */

/**
 * @file plugins/blocks/participant/index.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @ingroup plugins_blocks_participant
 * @brief Wrapper for "Participant" block plugin.
 *
 */

require_once('ParticipantBlockPlugin.inc.php');

return new ParticipantBlockPlugin();

?>
