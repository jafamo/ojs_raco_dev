{**
 * plugins/blocks/participant/block.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- "Participant" block.
 *}

<div class="pkp_block ">
	<div class="content">
		<!--<a href="{$baseUrl}" target="_blank">-->
		<a href="{$base_url}/raco" >
			<img src="{$base_url}/public/site/pageHeaderTitleImage_ca_ES.png" alt="Raco"/>
		</a>
	</div>
</div>
