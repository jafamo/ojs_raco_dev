<?php

/**
 * @file plugins/blocks/developedBy/DevelopedByBlockPlugin.inc.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class DevelopedByBlockPlugin
 * @ingroup plugins_blocks_developedBy
 *
 * @brief Class for "developed by" block plugin
 */



import('lib.pkp.classes.plugins.BlockPlugin');

class CertificadoBlockPlugin extends BlockPlugin {
	/**
	 * Determine whether the plugin is enabled. Overrides parent so that
	 * the plugin will be displayed during install.
	 * @param $contextId int Context ID (journal/press)
	 */
	function getEnabled($contextId = null) {
		if (!Config::getVar('general', 'installed')) return true;
		return parent::getEnabled($contextId);
	}

	/**
	 * Install default settings on system install.
	 * @return string
	 */
	function getInstallSitePluginSettingsFile() {
		return $this->getPluginPath() . '/settings.xml';
	}

	/**
	 * Install default settings on journal creation.
	 * @return string
	 */
	function getContextSpecificPluginSettingsFile() {
		return $this->getPluginPath() . '/settings.xml';
	}

	/**
	 * Get the block context. Overrides parent so that the plugin will be
	 * displayed during install.
	 * @param $contextId int optional
	 * @return int
	 */
	function getBlockContext($contextId = null) {
		if (!Config::getVar('general', 'installed')) return BLOCK_CONTEXT_SIDEBAR;
		return parent::getBlockContext($contextId);
	}

	/**
	 * Determine the plugin sequence. Overrides parent so that
	 * the plugin will be displayed during install.
	 * @param $contextId int Context ID (journal/press)
	 */
	function getSeq($contextId = null) {
		if (!Config::getVar('general', 'installed')) return 1;
		return parent::getSeq($contextId);
	}

	/**
	 * Get the display name of this plugin.
	 * @return String
	 */
	function getDisplayName() {
		return __('plugins.block.certificado.displayName');
	}

	/**
	 * Get a description of the plugin.
	 */
	function getDescription() {
		return __('plugins.block.certificado.description');
	}

	/**
	 * Obtindre el nom i el nivell del carhus
	 *
	 * @param [type] $templateMgr
	 * @param [type] $request
	 * @return void
	 */
	function getContents($templateMgr, $request = null){

		$journal = $request->getJournal();
		$locale = AppLocale::getLocale();
		//var_dump($locale); //ca_ES
		//$templateMgr->assign('isPostRequest',$request->isPost());
		$racocarhusDao = DAORegistry::getDAO('racocarhusDAO');
		$carhus = $racocarhusDao->getCarhusFromJournal($journal->_data['id']);

		//by default catalan
		$localeCarhus = "localeCA";

		if($locale === "es_ES"){
			$localeCarhus = "localeES";
		}else if($locale === "en_US"){
			$localeCarhus = "localeEN";
		}else{
			$localeCarhus = "localeCA";
		}

		$arraycarhus = array();
		
		foreach($carhus as $k=> $v){
			//print_r($v);
			$arraycarhus[] = array(
				'journal_id' => $v['journal_id'],
				'path' => $v['path'],
				'locale' => $v[$localeCarhus],
				'nivell' => $v['nivell']
			);
		}
		//echo "carhus desde el getcontents";
		//var_dump($arraycarhus);	
		$templateMgr->assign('arraycarhus', $arraycarhus);
		return parent::getContents($templateMgr, $request);
	
	}

}

?>
