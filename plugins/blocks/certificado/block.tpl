{**
 * plugins/blocks/certificado/block.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- "Certificado" block.
 *}


{assign var="baseWordpress" value="https://raco-pro-new.csuc.cat/wordpress-raco/"}

{if $currentLocale eq "es_ES"}
        {assign var="myLocale" value="es"}
        {assign var="pagina" value="-es"}
{elseif $currentLocale eq "en_ES"}
        {assign var="myLocale" value="en"}
        {assign var="pagina" value="-en"}
{else}
        {assign var="myLocale" value=""}
        {assign var="pagina" value=""}
{/if}



{if $currentJournal->getSetting('enableJcr') != 0  || $currentJournal->getSetting('enableFecyt') != 0 || count($arraycarhus) !=0}

        <div class="pkp_block ">
                <div class="content ">

                        {if $currentJournal->getSetting('enableJcr') != 0}
                                <!--<span class="title">Jcr</span> -->
                                <!--<a class="title" style="text-align:center; color:#a80724" {translate key="plugins.block.certificado.jcr"}</a>-->
				 <a href="{$base_url}/raco/index.php/jcr" class='title' style="text-align:center; color:#a80724" >{translate key="plugins.block.certificado.jcr"}</a>

                        {/if}                        
                        
                        {if $currentJournal->getSetting('enableFecyt') != 0}             
                                <a href="{$base_url}/raco/index.php/fecyt" class='title' style="text-align:center; color:#a80724" >{translate key="plugins.block.certificado.fecyt"}</a>
                        {/if}

{if count($arraycarhus) != 0}
 			<span class="btn-secundary title " color:#a80724 >
                                
                                <a href="{$baseWordpress}index.php/{$myLocale}/carhus{$pagina}">
                                {translate key="plugins.block.certificado.carhus"}<br> 
                                {foreach from=$arraycarhus item=item key=key name=name}                                        
                                        <span>{$item.locale}</span> : 
						{if $item.nivell eq 'A'}<span style="background-color:#689100; !important;" class="badge badge-pill">{$item.nivell}</span><br>{/if}
						{if $item.nivell eq 'B'}<span style="background-color:#52B4F1; !important" class="badge badge-pill ">{$item.nivell}</span><br>{/if}
						{if $item.nivell eq 'C'}<span style="background-color:#F67B04; !important" class="badge badge-pill ">{$item.nivell}</span><br>{/if}
						{if $item.nivell eq 'D'}<span style="background-color:#A80724 !important;" class="badge badge-pill badge-info">{$item.nivell}</span><br>{/if}
                                {/foreach}
                                </span>
                                </a>

{/if}






		</div>
	</div>

{/if}

