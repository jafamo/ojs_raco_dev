{**
 * plugins/blocks/languageToggle/block.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * Common site sidebar menu -- language toggle.
 *}
{if $enableLanguageToggle}
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<!-- <div class="pkp_block block_language"> -->
	

	
		<div style="text-align:center; padding-bottom: 20px;">
			{foreach from=$languageToggleLocales item=localeName key=localeKey}		
						
				<!-- <li style="color:red" class="locale_{$localeKey|escape}{if $localeKey==$currentLocale} current {/if}"> -->
			{if $localeKey == $currentLocale}  
				
					<a  style="color:red" href="{url router=$smarty.const.ROUTE_PAGE page="user" op="setLocale" path=$localeKey source=$smarty.server.REQUEST_URI}">
						{$localeName}	&nbsp					
					</a>
					{else}
					
					<a  href="{url router=$smarty.const.ROUTE_PAGE page="user" op="setLocale" path=$localeKey source=$smarty.server.REQUEST_URI}">
						{$localeName}&nbsp</i>
												
					</a>
					{/if}
				<i class="fas fa-grip-lines-vertical"></i>
			{/foreach}
		</div>
	

{/if}
