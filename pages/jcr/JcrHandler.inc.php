<?php


/**
 * Description of FecytHandler
 *
 * @author CSUC
 */
import('classes.handler.Handler');

class JcrHandler extends Handler {
	/**
	 * Display a list of the publishers availables on the site.
	 */
	function index($args,$request) {

           $templateMgr = TemplateManager::getManager($request); 
           $this->setupTemplate($request);
           
           $journal = $request->getJournal();
           $jcrDao =& DAORegistry::getDAO('JcrDAO');
           
           $misjournals = $jcrDao->getJournalsByJcrPrintIssnOnlineIssn();
           $jcrId='ALL'; 
           
            $templateMgr->assign(array(
                'jcrId'=>$jcrId,
                'misjournals' => $misjournals
                    ));
            
            $templateMgr->display('frontend/pages/Jcr.tpl');
             //$templateMgr->display('frontend/pages/fecyt.tpl');
    }
}
?>