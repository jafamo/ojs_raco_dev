<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FecytHandler
 *
 * @author jfarinos
 */
import('classes.handler.Handler');

class PublisherHandler extends Handler {
	/**
	 * Display a list of the publishers availables on the site.
	 */
    function index($args,$request) {            
        //$rangeInfo = Handler::getRangeInfo('publisher');
        $publisherDao =& DAORegistry::getDAO('PublisherDAO');
        //$publishers =& $publisherDao->getPublishers();
        $publishers = $publisherDao->getAllPublishers();
        
       $templateMgr = TemplateManager::getManager($request); 
       $this->setupTemplate($request);

       //$journal = $request->getJournal();	    
        $templateMgr->assign(array(                
                    'publishers' => $publishers
                    )); 

        //$templateMgr->display('admin/contextSettings.tpl');            
        $templateMgr->display('frontend/pages/publisher.tpl');            
    }

    //Buscar los publishers de una journal
    function myPublishers($args, $request){
        //definir el template
        $templateMgr = TemplateManager::getManager($request); 
        $this->setupTemplate($request);
       
        // obtener el ID de la journal
        $journal = $request->getJournal();		
        $journal_id = $journal->getId();

        $publisherDao =& DAORegistry::getDAO('PublisherDAO');
        $publishers = $publisherDao->getMyPublishers($journal_id);

        //var_dump($publishers);
        //die();
        //var_dump($publishers);
        //die();
        $templateMgr->assign(array(                
                    'publishers' => $publishers
                    )); 
        $templateMgr->display('frontend/pages/mypublisher.tpl');
    }
    
}
?>