<?php

/**
 * @file pages/management/SettingsHandler.inc.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class SettingsHandler
 * @ingroup pages_management
 *
 * @brief Handle requests for settings pages.
 */

// Import the base ManagementHandler.
import('lib.pkp.pages.management.ManagementHandler');

class SettingsHandler extends ManagementHandler {
	/**
	 * Constructor.
	 */
	function __construct() {
		parent::__construct();
		$this->addRoleAssignment(
			array(ROLE_ID_SITE_ADMIN),
			array(
				'access','tutorials',
			)
		);
		$this->addRoleAssignment(
			ROLE_ID_MANAGER,
			array(
				'settings',
				'publication',
				'distribution','tutorials',
			)
		);


		$this->addRoleAssignment(
			[ROLE_ID_MANAGER, ROLE_ID_SITE_ADMIN,
			ROLE_ID_SUB_EDITOR, ROLE_ID_AUTHOR,
			ROLE_ID_REVIEWER, ROLE_ID_ASSISTANT,
			ROLE_ID_READER, ROLE_ID_SUBSCRIPTION_MANAGER],
			array(				
				'allJournals','tutorials',
			)
		);


		
	}








	//
	// Public handler methods
	//
	/**
	 * Route to other settings operations.
	 * @param $args array
	 * @param $request PKPRequest
	 */
	function settings($args, $request) {
		$path = array_shift($args);
		switch($path) {
			case 'index':
			case '':
			case 'context':
				$this->journal($args, $request);
				break;
			case 'website':
				$this->website($args, $request);
				break;
			case 'publication':
				$this->publication($args, $request);
				break;
			case 'distribution':
				$this->distribution($args, $request);
				break;
			case 'access':
				$this->access($args, $request);
				break;
			case 'allJournals'://ADD CSUC
				$this->allJournals($args, $request);
				break;
			case 'tutorials'://ADD CSUC
				$this->tutorials($args, $request);
				break;
			default:
				assert(false);
		}
	}

	/**
	 * Display The Journal page.
	 * @param $args array
	 * @param $request PKPRequest
	 */
	function journal($args, $request) {
		$templateMgr = TemplateManager::getManager($request);
		$this->setupTemplate($request);

		// Display a warning message if there is a new version of OJS available
		if (Config::getVar('general', 'show_upgrade_warning')) {
			import('lib.pkp.classes.site.VersionCheck');
			if ($latestVersion = VersionCheck::checkIfNewVersionExists()) {
				$templateMgr->assign('newVersionAvailable', true);
				$templateMgr->assign('latestVersion', $latestVersion);
				$currentVersion = VersionCheck::getCurrentDBVersion();
				$templateMgr->assign('currentVersion', $currentVersion->getVersionString());

				// Get contact information for site administrator
				$roleDao = DAORegistry::getDAO('RoleDAO');
				$siteAdmins = $roleDao->getUsersByRoleId(ROLE_ID_SITE_ADMIN);
				$templateMgr->assign('siteAdmin', $siteAdmins->next());
			}
		}

		$templateMgr->display('management/settings/journal.tpl');
	}

	/**
	 * Display website page.
	 * @param $args array
	 * @param $request PKPRequest
	 */
	function website($args, $request) {
		$templateMgr = TemplateManager::getManager($request);
		$this->setupTemplate($request);
		$journal = $request->getJournal();
		$templateMgr->assign('enableAnnouncements', $journal->getSetting('enableAnnouncements'));
		$templateMgr->display('management/settings/website.tpl');
	}

	/**
	 * Display publication process page.
	 * @param $args array
	 * @param $request PKPRequest
	 */
	function publication($args, $request) {
		$templateMgr = TemplateManager::getManager($request);
		$this->setupTemplate($request);
		$templateMgr->display('management/settings/workflow.tpl');
	}

	/**
	 * Display distribution process page.
	 * @param $args array
	 * @param $request PKPRequest
	 */
	function distribution($args, $request) {
		$templateMgr = TemplateManager::getManager($request);
		$this->setupTemplate($request);
		AppLocale::requireComponents(LOCALE_COMPONENT_PKP_SUBMISSION); // submission.permissions
		$templateMgr->display('management/settings/distribution.tpl');
	}

	/**
	 * Display Access and Security page.
	 * @param $args array
	 * @param $request PKPRequest
	 */
	function access($args, $request) {
		$templateMgr = TemplateManager::getManager($request);
		$this->setupTemplate($request);
		$templateMgr->display('management/settings/access.tpl');
	}

	/**
	 * Display Alljournals CSUC
	 *
	 * @param $args $args
	 * @param $request $request
	 * @return void
	 */
	function allJournals($args, $request) {		
		$journalDao =& DAORegistry::getDAO('JournalDAO');
		$templateMgr = TemplateManager::getManager($request);
		$user =& $request->getUser();
		
		$roleDao = DAORegistry::getDAO('RoleDAO');
		//echo "<pre>";
		//print_r ($roleDao->getUsersByRoleId($user->getId()));
		//echo "</pre>";
		//die();	



		$journals = $journalDao-> getAllJournals($user->getId());
	
		$templateMgr->assign('journals', $journals);
		
		$this->setupTemplate($request);
		$templateMgr->display('management/settings/allJournals.tpl');
	}

	/**Crear el menu de tutorials CSUC
	 * 
	 */


	function tutorials($args, $request){
		$templateMgr = TemplateManager::getManager($request);
		$this->setupTemplate($request);
		$templateMgr->display('management/settings/tutorials.tpl');

	}



}

?>
