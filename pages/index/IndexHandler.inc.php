<?php

/**
 * @file pages/index/IndexHandler.inc.php
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @class IndexHandler
 * @ingroup pages_index
 *
 * @brief Handle site index requests.
 */

import('classes.handler.Handler');

class IndexHandler extends Handler {

	/**
	 * If no journal is selected, display list of journals.
	 * Otherwise, display the index page for the selected journal.
	 * @param $args array
	 * @param $request Request
	 */
	function index($args, $request) {
		$this->validate(null, $request);
		$journal = $request->getJournal();

		if (!$journal) {
			$journal = $this->getTargetContext($request, $journalsCount);
			if ($journal) {
				// There's a target context but no journal in the current request. Redirect.
				$request->redirect($journal->getPath());
			}
			if ($journalsCount === 0 && Validation::isSiteAdmin()) {
				// No contexts created, and this is the admin.
				$request->redirect(null, 'admin', 'contexts');
			}
		}

		




		$this->setupTemplate($request);
		$router = $request->getRouter();
		$templateMgr = TemplateManager::getManager($request);
		if ($journal) {

			//CSUC
		//publishers
		$publishersDAO = DAORegistry::getDAO('PublisherDAO');

        $journal_id = $journal->getId();
		$publishers = $publishersDAO->getMyPublishers($journal_id);


		$templateMgr->assign(array(                
			'publishers' => $publishers
			)); 



			// Assign header and content for home page
			$templateMgr->assign(array(
				'additionalHomeContent' => $journal->getLocalizedSetting('additionalHomeContent'),
				'homepageImage' => $journal->getLocalizedSetting('homepageImage'),
				'homepageImageAltText' => $journal->getLocalizedSetting('homepageImageAltText'),
				'journalDescription' => $journal->getLocalizedSetting('description'),
			));

			$issueDao = DAORegistry::getDAO('IssueDAO');
			$issue = $issueDao->getCurrent($journal->getId(), true);
			if (isset($issue) && $journal->getSetting('publishingMode') != PUBLISHING_MODE_NONE) {
				import('pages.issue.IssueHandler');
				// The current issue TOC/cover page should be displayed below the custom home page.
				IssueHandler::_setupIssueTemplate($request, $issue);
			}

			$enableAnnouncements = $journal->getSetting('enableAnnouncements');
			if ($enableAnnouncements) {
				$enableAnnouncementsHomepage = $journal->getSetting('enableAnnouncementsHomepage');
				if ($enableAnnouncementsHomepage) {
					$numAnnouncementsHomepage = $journal->getSetting('numAnnouncementsHomepage');
					$announcementDao = DAORegistry::getDAO('AnnouncementDAO');
					$announcements =& $announcementDao->getNumAnnouncementsNotExpiredByAssocId(ASSOC_TYPE_JOURNAL, $journal->getId(), $numAnnouncementsHomepage);
					$templateMgr->assign('announcements', $announcements->toArray());
					$templateMgr->assign('enableAnnouncementsHomepage', $enableAnnouncementsHomepage);
					$templateMgr->assign('numAnnouncementsHomepage', $numAnnouncementsHomepage);
				}
			}
                        /**CSUC 
                         * Comprovar que te =>1 paraules clau per mostrar keywordCloud
                         * per mostrar al theme de HealthScience RACO-PRO
                         * 
                         */
                        $paraulesClau=false;
                        $publishedArticleDao = DAORegistry::getDAO('PublishedArticleDAO');
                        $publishedArticles =& $publishedArticleDao->getPublishedArticlesByJournalId($journal->getId(), $rangeInfo = null, $reverse = true);
                        //Get all IDs of the published Articles
                        $submissionKeywordDao = DAORegistry::getDAO('SubmissionKeywordDAO');
                        $all_keywords = array();
                                      while ($publishedArticle = $publishedArticles->next()) {
                                              $article_keywords = $submissionKeywordDao->getKeywords($publishedArticle->getId(),
                                                      array(AppLocale::getLocale()))[AppLocale::getLocale()];
                                              if(count($article_keywords)>=1){
                                                  $paraulesClau=true;
                                                  break;
                                              }                                                			
                                      }
                        $templateMgr->assign('paraulesClau', $paraulesClau);
			$templateMgr->display('frontend/pages/indexJournal.tpl');
		} else {
			$journalDao = DAORegistry::getDAO('JournalDAO');
			$site = $request->getSite();

			if ($site->getRedirect() && ($journal = $journalDao->getById($site->getRedirect())) != null) {
				$request->redirect($journal->getPath());
			}

			$templateMgr->assign('pageTitleTranslated', $site->getLocalizedTitle());
			$templateMgr->assign('about', $site->getLocalizedAbout());
			$templateMgr->assign('journalFilesPath', $request->getBaseUrl() . '/' . Config::getVar('files', 'public_files_dir') . '/journals/');

			$journals = $journalDao->getAll(true);
			$templateMgr->assign('journals', $journals);
			$templateMgr->assign('site', $site);

			$templateMgr->setCacheability(CACHEABILITY_PUBLIC);
			$templateMgr->display('frontend/pages/indexSite.tpl');
		}
	}

	function estadistiques($args, $request){
		
		$this->setupTemplate($request);
		$router = $request->getRouter();
		$templateMgr = TemplateManager::getManager($request);
		
		$site = $request->getBaseUrl();
		$journal = $request->getJournal();
		$journal_id = $journal->getId();
		$path=$journal->getPath();


		$anios = array();
	    for($i = date("Y"); $i >=2006; $i--){
    	    $anios[] = $i;
    	}
    	
		$templateMgr->assign('path', $path);
		$templateMgr->assign('anios', $anios);
		$templateMgr->assign('router', $router);
		$templateMgr->assign('journal_id', $journal_id);
		$templateMgr->assign('journal', $jounal);
		$templateMgr->assign('site', $site);

		$templateMgr->display('frontend/pages/estadistiques.tpl');

	}







}

?>
