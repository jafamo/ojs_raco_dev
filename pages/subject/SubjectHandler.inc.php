<?php

/**
 * @file pages/subject/SubjectHandler.inc.php
 *
 * 2012 CESCA
 *
 * @class AdminPublisherHandler
 * @ingroup pages_admin
 *
 * @brief Handle requests for journal management in site administration.
 */

// $Id$
import('classes.subject.Subject');
import('classes.handler.Handler');

class SubjectHandler extends Handler {
	/**
	 * Display a list of the publishers availables on the site.
	 */
    function index() {
            //parent::validate();
            $templateMgr = &TemplateManager::getManager();
            $subjectDao = &DAORegistry::getDAO('SubjectDAO');
            $objects=&$subjectDao->getSubsubjects('');
            $n=0;
            foreach ($objects as $i){
                    $tmp=&$subjectDao->getSubsubjects($i[2]);
                    $num=&$subjectDao->countJournalsBySubject($i[0]);
                    $nummat=$num;
                    $subsubject[$n]= array();

                    $subsubject[$n][$i[0]]=array($i[0],$i[1],$i[2],$num);

                    foreach ($tmp as $k)
                    { $numsub=&$subjectDao->countJournalsBySubject($k[0]);
                      $subsubject[$n][$k[0]]=array($k[0],$k[1],$k[2],$numsub);
                      $num=$num+$numsub;
                    }
                    $subjects[$i[0]]=array($i[0],$i[1],$i[2],$subsubject[$n],$num,$nummat);
                    $n++;
            }

            $templateMgr->assign_by_ref('subjects', $subjects);
            $templateMgr->display('subject/index.tpl');
        }


    function getJournals($args){
            parent::validate();
            $templateMgr = &TemplateManager::getManager();
            $templateMgr->append('pageHierarchy', array(Request::url(null, 'subject', ''), 'navigation.subjects'));

            $subjectId = isset($args[0]) ? (int) $args[0] : 0;

            $journals= array();
            $subjectDao = &DAORegistry::getDAO('SubjectDAO');
            $subject = &$subjectDao->getSubject($subjectId);
            $journals=&$subjectDao->getJournalsBySubject($subjectId);
            $templateMgr->assign('subjectId',$subjectId);
            $templateMgr->assign_by_ref('subject', $subject->getTitle());
            $templateMgr->assign_by_ref('journals', $journals);
            $templateMgr->display('subject/journals.tpl');

    }

}
?>
