{strip}
{assign var="pageTitle" value="publisher"}
{assign var="pageCrumbTitle" value="publisher.browse"}
{include file="frontend/components/header.tpl" }
{/strip}

<h1>Pagina dels participants</h1>

<br />


 <div class="content">
    <h2>Llistat de Participants</h2>
    <table style="width:100%" class="listing">
    <tr class="heading">
            <td>ID</td>
            <td>Title</td>            
    </tr>
    
    <tr>
        <td colspan="2" class="headseparator">&nbsp;</td>
    </tr>
     {foreach from=$publishers  key=key item=item}
         <tr>
             <td style="width:10%;"> {$key}</td>
             <td style="width:80%;">{$item}</td>
         </tr>
   
    {/foreach}
 
        
    </table>
    </div>
  
{include file="common/footer.tpl"}