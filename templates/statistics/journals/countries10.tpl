{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.journal.statistics.journalCountries10"}
{include file="common/header.tpl"}

{if $countries10}
<img src="{$baseUrl}/img/statistics/journals/{$journalid}/paisos10_{$journalid}_{$currentLocale}.png"><br>


<table  width=100%>
<tr><td align=left>{if $previous}<a href="{$previous}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.journal.statistics.$previous}</a>{/if}</td><td align=right>

{if $next}<a href="journalTop30/{$next}" class="estadistiques">{translate key=navigation.journal.statistics.journalTop30.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}


{include file="common/footer.tpl"}

