{**
 * index.tpl
 *
 *}

{assign var="pageTitle" value="navigation.statistics"}
{include file="common/header.tpl"}


<table width="100%">
{if  $queries13m || $queries5y || $queriesAcu5y}
<tr valign="top">
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.statistics.journal.queries}</td>
<td align="right" >
{if $queries13m}<a href="{url op="journalQueries13m"}">{translate key=navigation.stats.queries.month}</a>{/if}
{if $queries13m && $queriesAcu5y} |  {/if}
{if $queriesAcu5y}<a href="{url op="journalQueriesAcu5y"}">{translate key=navigation.stats.queries.year}</a>{/if}
</td></tr>
{/if}


{if $countries10}
<tr><td colspan=3>&nbsp;</td></tr>
<tr valign="top">
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.statistics.journal.countries}</td>
<td align="right" >
<a href="{url op="journalCountries10"}">{translate key=navigation.statistics.journal.countries10}</a>
</td></tr>
{/if}
{* if $top30 || $top30_next *}
{if $top30_next}
<tr><td colspan=3>&nbsp;</td></tr>
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.stats.top30}</td>
<td align="right" >
{foreach from=$yearsList key=k item=year}
{if $year!=$smarty.now|date_format:"%Y"} |{/if} <a href="{url op="journalTop30"}/{$year}"}> {$year}</a>
{/foreach}
</td></tr>
{/if}

{foreach from=$publishers item=publisher}
<tr><td colspan=3>&nbsp;</td></tr>
<tr valign="top">
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.statistics.journal.publisher} 
</td><td align="right" >
<a href="{$baseUrl}/index.php/index/publisher/statistics/{$publisher.id}" class="action">{$publisher.title}</a><br>
</td></tr>

{/foreach}


</table>


<br><br>
{translate key=navigation.statistics.description}


{include file="common/footer.tpl"}

