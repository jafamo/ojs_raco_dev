{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.estadistiques"}
{include file="common/header.tpl"}

{if $consultes5y}
<h2>{translate key="navigation.journal.stats.consultes5y"}</h2>
<img src="{$baseUrl}/img/estadistiques/journals/{$journalid}/consultes5y_{$journalid}_{$currentLocale}.png"><br>

<table  width=100%>
<tr><td align=left>{if $previous}<a href="{$pageUrl}/about/{$previous}">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.journal.stats.$previous}</a>{/if}</td><td align=right>{if $next}<a href="{$pageUrl}/about/{$next}">{translate key=navigation.journal.stats.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}

{include file="common/footer.tpl"}

