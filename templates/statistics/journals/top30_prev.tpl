{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.journal.statistics.journalTop30.$any"}
{include file="common/header.tpl"}
{assign var="nextYear" value=$any-1}
{if $any<2013} {assign var="prevYear" value=$any+1}{/if}
{if $top30_prev}
{include file="statistics/journals/top30/llistaTop30ByJournal_"|cat:"$any"|cat:"_"|cat:"$journalid"|cat:"_$currentLocale.tpl"}
<br>

<table  width=100%>
<tr><td align=left>

{if $prevYear}
<a href="{$prevYear}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.journal.statistics.journalTop30.$prevYear}</a>
{else}
{if $previous}<a href="../{$previous}" class="estadistiques">
 <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.journal.statistics.$previous}</a>{/if}
{/if}
</td><td align=right>{if $next}<a href="{$nextYear}" class="estadistiques">{translate key=navigation.journal.statistics.journalTop30.$nextYear} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}
{include file="common/footer.tpl"}

