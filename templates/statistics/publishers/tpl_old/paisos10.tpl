{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.inst.stats.Paisos.Long"}
{include file="common/header.tpl"}

{if $paisos10}
<h4>{$institucio}</h4>

<img src="{$baseUrl}/img/estadistiques/institucions/{$institucioId}/paisos10_{$institucioId}_{$currentLocale}.png"><br>



<table  width=100%>
<tr><td align=left>

{if $previous}<a href="{$baseUrl}/index.php/index/raco/{$previous}/{$institucioId}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.inst.stats.$previous}</a>{/if}


</td><td  align=right>{if $next}<a href="{$baseUrl}/index.php/index/raco/{$next}/{$institucioId}" class="estadistiques">{translate key=navigation.inst.stats.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}


{include file="common/footer.tpl"}

