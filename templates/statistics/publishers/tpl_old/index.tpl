{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.estadistiques"}
{include file="common/header.tpl"}
<h4>{$inst}</h4>
<br>
<table width="100%">
{if  $consultes13m || $consultes5y || $consultes5yt}
<tr valign="top">
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.stats.consultats"}</td>
<td align="right" >
{if $consultes13m}<a href="{$baseUrl}//index.php/index/raco/Consultes13m/{$institucioId}"}>{translate key=navigation.stats.consultats.mesos}</a>{/if}
{if $consultes13m && $consultes5yt} |  {/if}
{if $consultes5yt}<a href="{$baseUrl}/index.php/index/raco/ConsultesAcu5y/{$institucioId}"}>{translate key=navigation.stats.consultats.anys.anual}</a>{/if}
</td></tr>
{/if}

{if $paisos || $paisos10}
<tr><td colspan=3>&nbsp;</td></tr>
<tr valign="top">
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.inst.stats.paisos}</td>
<td align="right" >
{if $paisos10} <a href="{$baseUrl}/index.php/index/raco/Paisos10/{$institucioId}">{translate key=navigation.inst.stats.paisos.total}</a>{/if}
{if $paisos && $paisos10} | {/if}
{if $paisos}<a href="{$baseUrl}/index.php/index/raco/Paisos/{$institucioId}">{translate key=navigation.inst.stats.paisos.revista}</a>{/if}
</td></tr>
{/if}
{if $top30 || $top30anterior}

<tr><td colspan=3>&nbsp;</td></tr>
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.stats.top30}</td>
<td align="right" >
{if $top30} <a href="{$baseUrl}/index.php/index/raco/top30/{$institucioId}">{$smarty.now|date_format:"%Y"}</a>{/if}
{if $top30anterior}
{foreach from=$llistanys key=k item=any}
{if $top30} |{/if} <a href="{$baseUrl}/index.php/index/raco/top30_anterior/{$any}/{$institucioId}"> {$any}</a>
{/foreach}
{/if}
</td></tr>
{/if}


</table>
<br><br>
{translate key=navigation.stats.explicacio}

{include file="common/footer.tpl"}

