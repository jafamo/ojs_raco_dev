{**
 * estadistiques.tpl
 *
 *}

{*assign var="pageTitle" value="navigation.estadistiques"*}

{assign var="pageTitle" value="navigation.inst.stats.Consultes13m"}

{include file="common/header.tpl"}

{if $consultes13m}
<h4>{$institucio}</h4>
<img src="{$baseUrl}/img/estadistiques/institucions/{$institucioId}/consultes13m_{$institucioId}_{$currentLocale}.png"><br>

<table  width=100%>
<tr><td align=left>&nbsp;</td><td align=right>{if $next}<a href="{$baseUrl}/index.php/index/raco/ConsultesAcu5y/{$institucioId}" class="estadistiques">{translate key=navigation.inst.stats.consultes5y} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}

{include file="common/footer.tpl"}

