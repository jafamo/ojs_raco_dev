{**
 * estadistiques.tpl
 *
 *}





{assign var="pageTitle" value="navigation.inst.stats.consultes5y"}
{include file="common/header.tpl"}
{if $consultes5y}
<h4>{$institucio}</h4>
<br>	  
{include file="raco/estadistiquesinstitucio/consultesAcu5y_"|cat:"$institucioId"|cat:"_"|cat:"$currentLocale"|cat:".tpl"}

<br><sup>*</sup>{translate key="navigation.stats.consultats.5anys.update"}

<table  width=100%>
<tr><td align=left>{if $previous}<a href="{$baseeUrl}/index.php/index/raco/{$previous}/{$institucioId}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.inst.stats.$previous}</a>{/if}</td><td align=right>{if $next}<a href="{$baseUrl}/index.php/index/raco/{$next}/{$institucioId}" class="estadistiques">{translate key=navigation.inst.stats.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}
{include file="common/footer.tpl"}

