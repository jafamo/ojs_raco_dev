{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.inst.stats.top30.anterior.$any"}
{include file="common/header.tpl"}

{assign var="seguent" value=$any-1}
{if $any<2011} {assign var="anterior" value=$any+1}{/if} 

{if $top30_anterior}
<h4>{$inst}</h4>
 {if $currentLocale eq "ca_ES" ||  $currentLocale eq "es_ES"} 
{include file="raco/estadistiquesinstitucio/llistaTop30ByInst_"|cat:"$any"|cat:"_"|cat:"$institucioId"|cat:"_ca_ES.tpl"}

  {else}
{include file="raco/estadistiquesinstitucio/llistaTop30ByInst_"|cat:"$any"|cat:"_"|cat:"$institucioId"|cat:"_en_US.tpl"}
  {/if}

{/if}


<table  width=100%>
<tr><td align=left>
{if $anterior}
<a href="{$pageUrl}/raco/top30_anterior/{$anterior}/{$institucioId}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.inst.stats.top30.anterior.$anterior}</a>
{else}
{if $previous}<a href="{$pageUrl}/raco/{$previous}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.inst.stats.$previous}</a>{/if}
{/if}
</td><td align=right>{if $next}<a href="{$pageUrl}/raco/top30_anterior/{$seguent}/{$institucioId}" class="estadistiques">{translate key=navigation.inst.stats.top30.anterior.$seguent} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>


{include file="common/footer.tpl"}

