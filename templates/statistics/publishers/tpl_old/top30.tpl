{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.inst.stats.top30"}
{include file="common/header.tpl"}


{if $top30}
<h4>{$inst}</h4>
 {if $currentLocale eq "ca_ES" ||  $currentLocale eq "es_ES"}
{include file="raco/estadistiquesinstitucio/llistaTop30ByInst_2012_$institucioId"|cat:"_ca_ES.tpl"}
{else}
{include file="raco/estadistiquesinstitucio/llistaTop30ByInst_2012_$institucioId"|cat:"_en_US.tpl"}
{/if}
<br>
<table  width=100%>
<tr><td align=left>{if $previous}<a href="{$pageUrl}/raco/{$previous}/{$institucioId}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.inst.stats.$previous}</a>{/if}</td><td align=right>{if $next}<a href="{$pageUrl}/raco/{$next}/2010/{$institucioId}" class="estadistiques">{translate key=navigation.inst.stats.top30.anterior.2010} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}

{include file="common/footer.tpl"}

