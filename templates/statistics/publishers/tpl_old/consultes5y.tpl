{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.inst.stats.consultes5y"}
{include file="common/header.tpl"}

{if $consultes5y}
<h4>{$institucio}</h4>
<img src="{$baseUrl}/img/estadistiques/institucions/{$institucioId}/consultes5y_{$institucioId}_{$currentLocale}.png"><br>

<table  width=100%>
<tr><td align=left>{if $previous}<a href="{$baseUrl}/index.php/index/raco/{$previous}/{$institucioId}">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.institucio.stats.$previous}</a>{/if}</td><td align=right>{if $next}<a href="{$baseUrl}/index.php/index/raco/{$next}/{$institucioId}">{translate key=navigation.institucio.stats.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}

{include file="common/footer.tpl"}

