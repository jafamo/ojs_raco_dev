{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.publisher.statistics.$year"}
{include file="common/header.tpl"}

{assign var="seguent" value=$year-1}

{if $year<date('Y')} {assign var="anterior" value=$year+1}{/if} 
{if $top30_prev}
<h4>{$publisher}</h4>
 {if $currentLocale eq "ca_ES" ||  $currentLocale eq "es_ES"} 
{include file="statistics/publishers/top30/llistaTop30ByInst_"|cat:"$year"|cat:"_"|cat:"$publisherId"|cat:"_ca_ES.tpl"}

  {else}
{include file="statistics/publishers/top30/llistaTop30ByInst_"|cat:"$year"|cat:"_"|cat:"$publisherId"|cat:"_en_US.tpl"}
  {/if}

{/if}


<table  width=100%>
<tr><td align=left>
{if $anterior<>0}

<a href="{$baseUrl}/index.php/index/publisher/top30_prev/{$anterior}/{$publisherId}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.publisher.statistics.$anterior}</a>
{else}

{if $previous}<a href="{$pageUrl}/index.php/index/publisher/{$previous}/{$publisherId}" class="estadistiques">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.publisher.statistics.$previous}</a>{/if}
{/if}
</td><td align=right>{if $next}<a href="{$pageUrl}/index.php/index/publisher/top30_prev/{$seguent}/{$publisherId}" class="estadistiques">{translate key=navigation.publisher.statistics.$seguent} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>


{include file="common/footer.tpl"}

