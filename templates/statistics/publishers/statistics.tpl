{**
 * publisher/statistics.tpl
 *
 *}

{assign var="pageTitle" value="navigation.statistics"}
{include file="common/header.tpl"}
<h4>{$publisherTitle}</h4>
<br>
<table width="100%">
{if  $queries13m|| $queries5y || $queries5yt}
<tr valign="top">
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.publisher.statistics.queries"}</td>
<td align="right" >
{if $queries13m}<a href="{url op="queries13m"}/{$publisherId}">{translate key=navigation.publisher.statistics.queries.month}</a>{/if}
{if $queries13m && $queries5yt} |  {/if}
{if $queries5yt}<a href="{$baseUrl}/index.php/index/publisher/queries5yt/{$publisherId}"}>{translate key=navigation.publisher.statistics.year}</a>{/if}
</td></tr>
{/if}

{if $countries || $countries10}
<tr><td colspan=3>&nbsp;</td></tr>
<tr valign="top">
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.publisher.statistics.countries}</td>
<td align="right" >
{if $countries10} <a href="{$baseUrl}/index.php/index/publisher/countries10/{$publisherId}">{translate key=navigation.publisher.statistics.countries.10}</a>{/if}
{if $countries && $countries10} | {/if}
{if $countries}<a href="{$baseUrl}/index.php/index/publisher/countries/{$publisherId}">{translate key=navigation.publisher.statistics.countries.10.journals}</a>{/if}
</td></tr>
{/if}
{if $top30 || $top30_prev}

<tr><td colspan=3>&nbsp;</td></tr>
<td width="2%" ><span style="font-size:1.2em;font-style:bold;color:#8b121e;">&#8226;</span></td>
<td width="43%" >{translate key=navigation.publisher.statistics.top30}</td>
<td align="right" >
{if $top30} <a href="{$baseUrl}/index.php/index/publisher/top30_prev/{$current}/{$publisherId}">{$smarty.now|date_format:"%Y"}</a>{/if}
{if $top30_prev}
{foreach from=$llistanys key=k item=any}
{if $top30} |{/if} <a href="{$baseUrl}/index.php/index/publisher/top30_prev/{$any}/{$publisherId}"> {$any}</a>
{/foreach}
{/if}
</td></tr>
{/if}

</table>
<br><br>
{translate key=navigation.statistics.description}

{include file="common/footer.tpl"}

