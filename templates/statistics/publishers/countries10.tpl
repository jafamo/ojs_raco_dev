{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.publisher.statistics.countries"}

{include file="common/header.tpl"}
{if $countries10}
<h4>{$publisher}</h4>
<img src="{$baseUrl}/img/statistics/publishers/{$publisherId}/paisos10_{$publisherId}_{$currentLocale}.png"><br>

<table  width=100%>
<tr><td align=left>{if $previous}<a href="{$baseUrl}/index.php/index/publisher/{$previous}/{$publisherId}">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.publisher.statistics.$previous}</a>{/if}</td><td align=right>{if $next}<a href="{$baseUrl}/index.php/index/publisher/{$next}/{$publisherId}">{translate key=navigation.publisher.statistics.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>

</table>

{/if}

{include file="common/footer.tpl"}

