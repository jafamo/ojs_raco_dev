{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.publisher.statistics.queries5yt"}

{include file="common/header.tpl"}

{if $queries5yt}
<h4>{$publisher}</h4>
{include file="statistics/publishers/queries/consultesAcu5y_"|cat:"$publisherId"|cat:"_"|cat:"$currentLocale"|cat:".tpl"}

<table  width=100%>
<tr><td align=left>{if $previous}<a href="{$baseUrl}/index.php/index/publisher/{$previous}/{$publisherId}">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.publisher.statistics.$previous}</a>{/if}</td><td align=right>{if $next}<a href="{$baseUrl}/index.php/index/publisher/{$next}/{$publisherId}">{translate key=navigation.publisher.statistics.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>

</table>

{/if}

{include file="common/footer.tpl"}
