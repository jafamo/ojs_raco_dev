{**
 * estadistiques.tpl
 *
 *}

{assign var="pageTitle" value="navigation.publisher.statistics.queries13m"}

{include file="common/header.tpl"}

{if $queries13m}
<h4>{$publisher}</h4>
<img src="{$baseUrl}/img/statistics/publishers/{$publisherId}/consultes13m_{$publisherId}_{$currentLocale}.png"><br>

<table  width=100%>
<tr><td align=left>&nbsp;</td><td align=right>{if $next}<a href="{$baseUrl}/index.php/index/publisher/{$next}/{$publisherId}" class="estadistiques">{translate key=navigation.publisher.statistics.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}

{include file="common/footer.tpl"}

