{**
 * estadistiques.tpl
 *
 *}
{assign var="pageTitle" value="navigation.publisher.statistics.top30"}

{include file="common/header.tpl"}

{if $top30}
<h4>{$publisher}</h4>
 {if $currentLocale eq "ca_ES" ||  $currentLocale eq "es_ES"}
{include file="statistics/publishers/top30/llistaTop30ByInst_2012_$publisherId"|cat:"_ca_ES.tpl"}
{else}
{include file="statistics/publishers/top30/llistaTop30ByInst_2012_$publisherId"|cat:"_en_US.tpl"}
{/if}
<br>
<table  width=100%>

<tr><td align=left>{if $previous}<a href="{$baseUrl}/index.php/index/publisher/{$previous}/{$publisherId}">   <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#171;</span>{translate key=navigation.publisher.statistics.$previous}</a>{/if}</td><td align=right>{if $next}<a href="{$baseUrl}/index.php/index/publisher/{$next}/{$publisherId}">{translate key=navigation.publisher.statistics.$next} <span style="font-size:1.5em;font-style:bold;color:#8b121e;">&#187;</span></a>{/if}</td></tr>
</table>

{/if}

{include file="common/footer.tpl"}

