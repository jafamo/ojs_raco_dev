{strip}
{assign var="pageTitle" value="journal.myJournals"}
{include file="common/header.tpl"}
{/strip}


<div id="contexts">
{url|assign:"homeUrl" page="submission" router=$smarty.const.ROUTE_PAGE}

	<ul>
		{foreach from=$journals key=key item=journal}			
		<li>
			<a  href="{$baseUrl}/{"index.php"|escape}/{$journal.path}/{"submissions"|escape}">{$journal.name}</a>
			<br>
		</li>
		{/foreach}
	</ul>
</div>

{include file="common/footer.tpl"}