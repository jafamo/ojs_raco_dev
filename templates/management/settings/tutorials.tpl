{**
 * templates/management/settings/tutorials.tpl
 *
 * Copyright (c) 2014-2019 Simon Fraser University
 * Copyright (c) 2003-2019 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *

 *}


{strip}
{assign var="pageTitle" value="admin.tutorials"}
{include file="common/header.tpl"}
{/strip}

<div class="distributionTabs" style="padding-left:20px;">

    <h1>Tutorials del OJS3</h1>

    <a target="_blank" href="https://pkpschool.sfu.ca/courses/setting-up-a-journal-in-ojs-3/" >Setting up a Journal in OJS 3 </a>
    <br>
    <a target="_blank" href="https://pkpschool.sfu.ca/courses/editorial-workflow-in-ojs-3/">Editorial Workflow in OJS 3</a>
    <br>
    <a target="_blank" href="https://pkpschool.sfu.ca/courses/configuracion-de-una-revista-en-ojs-3/">Configuración de una revista en OJS 3</a>
    <br>
    <a target="_blank" href="https://pkpschool.sfu.ca/courses/flujo-de-trabajo-editorial-en-ojs-3/">Flujo de trabajo editorial en OJS 3</a>

</div>

{include file="common/footer.tpl"}

