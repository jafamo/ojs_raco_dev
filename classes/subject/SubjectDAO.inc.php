<?php

/**
 * SubjectDAO.inc.php
 *
 * Copyright (c) 2012 CESCA
*
*/

import ('classes.subject.Subject');

class SubjectDAO extends DAO {

        /**
         * Constructor.
         */                
        function __constructor() {            
            parent::DAO();           
        }

        function getSubjects($rangeInfo = null) {
                 $result = &$this->retrieveRange('SELECT * FROM subjects ORDER BY code',
                        false, $rangeInfo
                );

                $returner = new DAOResultFactory($result, $this, '_returnSubjectFromRow');
                return $returner;
        }

        function &_returnSubjectFromRow(&$row) {
                $subject = new Subject();
                $subject->setSubjectId($row['subject_id']);
                $subject->setTitle($row['title_ca_es']);
                $subject->setTitle1($row['title_es_es']);
                $subject->setTitle2($row['title_en_us']);
                $subject->setCode($row['code']);
                $subject->setSubsubject($row['subsubject']);
                $subject->setSequence($row['seq']);
                $subject->setEnabled($row['enabled']);

                HookRegistry::call('SubjectDAO::_returnSubjectFromRow', array(&$subject, &$row));

                return $subject;
        }

        function insertSubject(&$subject) {
                $this->update(
                        'INSERT INTO subjects 
                                (code, title_ca_ES,title_es_ES, title_en_US, subsubject,seq,enabled)
                                VALUES
                                (?, ?, ?, ?, ?, ?, ?)',
                        array(
                                $subject->getCode(),
                                $subject->getTitle(),
                                $subject->getTitle1(),
                                $subject->getTitle2(),
                                $subject->getSubsubject(),
                                $subject->getSequence() == null ? 0 : $subject->getSequence(),
                                $subject->getEnabled() ? 1 : 0
                        )
                );

                $subject->setSubjectId($this->getInsertSubjectId());
                return $subject->getSubjectId();
        }

        function getInsertSubjectId() {
                return $this->getInsertId('subjects', 'subject_id');
        }

        function resequenceSubjects() {
                $result = &$this->retrieve(
                        'SELECT subject_id FROM subjects ORDER BY seq'
                );
                for ($i=1; !$result->EOF; $i++) {
                        list($subjectId) = $result->fields;
                        $this->update(
                                'UPDATE subjects SET seq = ? WHERE subject_id = ?',
                                array(
                                        $i,
                                        $subjectId
                                )
                        );

                        $result->moveNext();
                }

                $result->close();
                unset($result);
        }

        function &getSubject($subjectId) {
                $result = &$this->retrieve(
                        'SELECT * FROM subjects WHERE subject_id = ?', $subjectId
                );

                $returner = null;
                if ($result->RecordCount() != 0) {
                        $returner = &$this->_returnSubjectFromRow($result->GetRowAssoc(false));
                }
                $result->Close();
                unset($result);
                return $returner;
        }

        function updateSubject(&$subject) {
                return $this->update(
                        'UPDATE subjects
                                SET
                                        title_ca_ES = ?,
                                        title_es_ES = ?,
                                        title_en_US = ?,
                                        code = ?,
                                        subsubject = ?,
                                        seq = ?,
                                        enabled = ?
                                WHERE subject_id = ?',
                        array(
                                $subject->getTitle(),
                                $subject->getTitle1(),
                                $subject->getTitle2(),
                                $subject->getCode(),
                                $subject->getSubsubject(),
                                $subject->getSequence(),
                                $subject->getEnabled() ? 1 : 0,
                                $subject->getSubjectId()
                        )
                );
        }

        function deleteSubjectById($subjectId) {
                return $this->update(
                        'DELETE FROM subjects WHERE subject_id = ?', $subjectId
                );
        }

        function &getSubsubjects($arg){
            $subjects = array();
            $idioma=Locale::getLocale();

            $subjectDao = &DAORegistry::getDAO('SubjectDAO');


            $result = &$this->retrieve('SELECT subject_id,title_'.$idioma.',subsubject,code  FROM subjects WHERE subsubject=?',$arg);
            while (!$result->EOF) {
                $subjectId = $result->fields[0];
                $journals=$subjectDao->getJournalsBySubject($subjectId);
                $subjects[$subjectId] =array($subjectId,$result->fields[1],$result->fields[3],$journals);

                $result->moveNext();
            }
            $result->Close();
            return $subjects;
        }

        function &countJournalsBySubject($arg){
            
            $journals = array();
            $journalDao = &DAORegistry::getDAO('SubjectDAO');
            $result = &$this->retrieve('SELECT s.journal_id,s.setting_value, s.setting_type 
                                        FROM journal_settings s, journals j 
                                        WHERE j.journal_id=s.journal_id and s.setting_name=\'subjects\' and j.enabled=1'                    
                                       );
            $num=0;
            while (!$result->EOF) {
                $row = &$result->getRowAssoc(false);
                $value = unserialize($row['setting_value']);
               foreach($value as $i){
                       if(strcmp($arg,$i['name'])==0) {
                                $num++;
                       }
                }
                $result->MoveNext();
            }   
            $result->Close();
            return $num;
         }


        function getJournalsBySubject($arg){
            
            $journals = array();
            $journalDao = DAORegistry::getDAO('JournalDAO');

            $result = &$this->retrieve('SELECT s.journal_id,s.setting_value, s.setting_type 
                                        FROM journal_settings s, journals j 
                                        WHERE j.journal_id=s.journal_id and s.setting_name=\'subjects\' and j.enabled=1 '
                                        );
            while (!$result->EOF) {                
                $row = &$result->getRowAssoc(false);
                $value = unserialize($row['setting_value']);
               foreach($value as $i){
                       if(strcmp($arg,$i['name'])==0) {
                                $journalId=$row['journal_id'];
                                $journals[$journalId]=$journalDao->getJournal($journalId);
                       }
                }

                $result->MoveNext();
            }
            $result->Close();
            return $journals;
        }

        function &getSubjectsSuggestion(){ 
            $subjects = '';
            $idioma=Locale::getLocale();
            $subjectDao = &DAORegistry::getDAO('SubjectDAO');
            $result = &$this->retrieve('SELECT title_'.$idioma.'  FROM subjects order by title_'.$idioma);
            while (!$result->EOF) {
                $subjects = $subjects ."\"" . $result->fields[0] . "\", ";
                $result->moveNext();
            }
            $result->Close();

            return $subjects;
        }


        function &getSubjectByTitle($title) {
                $returner = null;
	        $idioma=Locale::getLocale();

                $result =& $this->retrieve(
                        'SELECT * FROM subjects WHERE LOWER(title_'.$idioma.') = ? ', $title
                );

                if ($result->RecordCount() != 0) {
                        $returner =& $this->_returnSubjectFromRow($result->GetRowAssoc(false));
                }
                $result->Close();
                unset($result);
                return $returner;
        }

        function getSubjectInfo($arg){ 
            $materia = array();

            $idioma=Locale::getLocale();
            $result = &$this->retrieve('SELECT code,title_'.$idioma.' AS title, subsubject FROM subjects WHERE subject_id=?',$arg);
            while (!$result->EOF) {
                $row = &$result->getRowAssoc(false);
                $materia=array("subject_id"=>$arg,"code"=>$row['code'],"name"=>$row['title'],"subsubject"=>$row['subsubject']);
                //al select materia id i al array $arg
                $result->MoveNext();
            }
            return  $materia;
        }
        
        /* obtindre tots esl subject de la BD con la locale en particular.
         *  
         */
        function getAllSubjects() {
            $subjects = array();
            $idioma = AppLocale::getLocale();  // EN OJS 3                      
                     
            $result = $this->retrieveRange('SELECT subject_id,code,title_'.$idioma.' FROM subjects ORDER BY code');
            
            while(!$result->EOF){
                //$publishers = $result->GetRowAssoc(false);               
                $subject_id = $result->fields[0];                 
                $code = $result->fields[1];                  
                $title_lang = $result->fields[2];                    
                //$subSubject = $result->fields[3];
                
                //echo $subSubject."<br/>";
                
                $subjects[$subject_id] = array(
                            'code' => $code,
                            'title_lang' => $title_lang
                    );                     
                $result->moveNext();                                                
                
            }                      
            return $subjects;
        }
       
        /*Obtindre els subjects que te una journal
         * 
         */
        function getSubjectByJournal(){
            
            
        }
        
        
        
}



?>

