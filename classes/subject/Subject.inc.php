<?php

/**
 * Subject.inc.php
 *
 * Copyright (c) 20i12 CESCA
*
*/

class Subject extends DataObject {

	/**
	 * Constructor.
	 */
	function Subject() {
		parent::DataObject();
	}
	
	function getSubjectId() {
		return $this->getData('materia_id');
	}

	function setSubjectId($materia_id) {
		return $this->setData('materia_id', $materia_id);
	}

        function getTitle() {
                return $this->getData('title_ca_ES');
        }

	function setTitle($title) {
		return $this->setData('title_ca_ES',$title);
	}

        function getTitle1() {
                return $this->getData('title_es_ES');
        }

        function setTitle1($title) {
                return $this->setData('title_es_ES',$title);
        }

        function getTitle2() {
                return $this->getData('title_en_US');
        }

        function setTitle2($title) {
                return $this->setData('title_en_US',$title);
        }

        function getCode() {
                return $this->getData('code');

        }

	function setCode($code) {
		return $this->setData('code',$code);
	}	

        function getSubsubject() {
                return $this->getData('subsubject');

        }

        function setSubsubject($submateria) {
                return $this->setData('subsubject',$submateria);
        }

	 function getEnabled() {
	 	return $this->getData('enabled');
	}
	
	function setEnabled($enabled) {
		return $this->setData('enabled',$enabled);
	}

	function getSequence() {
		return $this->getData('sequence');
	}
	
	function setSequence($sequence) {
		return $this->setData('sequence', $sequence);
	}


}
?>

