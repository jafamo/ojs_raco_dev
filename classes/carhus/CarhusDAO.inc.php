<?php
/**
 * CarhusDAO.inc.php
 *
 * Copyright (c) 2018 CSUC
*
*/

import('classes.journal.Journal');

class CarhusDAO extends DAO {
       
    function __constructor() {
        parent::DAO();    
        echo "constructor de Carhus";    
            die();
            //$this->getJournalsByFecyt();
            
           
        }


    /**
     * Obtenir el carhus de una journal
     * @author Javier Farinos  <javier.farinos@csuc.cat>
     * 
     */

    public function getCarhusByJournal($journal_id){

        $result = $this->retrieve(
                        'SELECT distinct js.journal_id as journal_id,js.setting_value as path, 
                                c.locale_ca as locale_ca, c.locale_en as locale_en, c.locale_es as locale_es, cs.nivell as nivell
                        FROM carhus_settings as cs, journal_settings as js, carhus as c
                        WHERE cs.journal_id = js.journal_id
                        AND c.carhus_id = cs.carhus_id
                        AND js.setting_name = "name"
                        AND js.journal_id=?',
                    $journal_id);
        
                    $carhus = array();

        while(!$result->EOF){            
            $journalId = $result->fields['journal_id'];
            $journalPath = $result->fields['path'];
            $localeCA = $result->fields['locale_ca'];
            $localeES = $result->fields['locale_es'];
            $localeEN = $result->fields['locale_en'];
            $nivel = $result->fields['nivell'];
            $carhus[$journal_id] = array(
                                        'journal_id' => $journalDao->_fromRow($journalId),
                                        'path' => $journalPath,
                                        'localeCA' => $localeCA,
                                        'localeES' => $localeES,
                                        'localeEN' => $localeEN,
                                        'nivell' => $nivel

            );
            $result->moveNext();
        }
        return $carhus;
    }
    
}
?>