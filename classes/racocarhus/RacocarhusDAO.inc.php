<?php

/**
 * RacocarhusDAO.inc.php
 * @author Javier Farinos <javier.farinos@csuc.cat>
 * Copyright (c) 2019 CSUC
*
*/
//import('lib.pkp.classes.context.ContextDAO');
import('classes.journal.Journal');
import('classes.journal.JournalDAO');

class RacocarhusDAO extends DAO {

    function __constructor() {
            parent::DAO();           
        }
    function getCarhusFromJournal($journalId){
        
        
$result = $this->retrieve('SELECT  cs.journal_id,  c.locale_ca, c.locale_en, c.locale_es, cs.nivell 
				FROM carhus_settings as cs, carhus as c 
				WHERE  c.carhus_id = cs.carhus_id  
					 AND cs.journal_id=? ',$journalId);

/*$result = $this->retrieve('SELECT distinct js.journal_id,js.setting_value as path, 
                                            c.locale_ca, c.locale_en, c.locale_es, cs.nivell
                                    FROM carhus_settings as cs, journal_settings as js, carhus as c
                                    WHERE cs.journal_id = js.journal_id
                                        AND c.carhus_id = cs.carhus_id
                                        AND js.setting_name = "name"
                                       AND js.journal_id=? ',$journalId);  
*/
        $carhus = array();
        $journalDao =& DAORegistry::getDAO('JournalDAO');

        while(!$result->EOF){            
            $journalId = $result->fields['journal_id'];
            //$journalPath = $result->fields['path'];
            $localeCA = $result->fields['locale_ca'];
            $localeES = $result->fields['locale_es'];
            $localeEN = $result->fields['locale_en'];
            $nivel = $result->fields['nivell'];
            $carhus[] = array(
                        'journal_id' => $journalId,
                        //'path' => $journalPath,
                        'localeCA' => $localeCA,
                        'localeES' => $localeES,
                        'localeEN' => $localeEN,
                        'nivell' => $nivel
            );
            $result->moveNext();
        }
       
        return $carhus;
    }
}

?>
