<?php

/**
 * @file classes/publisher/PublisherDAO.inc.php
 *
 * 2012 CESCA
 *
 * @class PublisherDAO
 * @ingroup publisher
 * @see Publisher
 *
 * @brief Operations for retrieving and modifying Journal objects.
 */

// $Id$


import ('classes.publisher.Publisher');

class PublisherDAO extends DAO {
    
    /**
     * 
     */
        function __constructor() {
            echo "constructor de Publisher";    
            //$this->getJournalsByFecyt();
            parent::DAO();
           
        }
	/**
	 * Retrieve a publisher by ID.
	 * @param $publisherId int
	 * @return Publisher
	 */
	function getPublisher($publisherId) {
		$result =& $this->retrieve(
			'SELECT * FROM publishers WHERE publisher_id = ?', $publisherId
		);
		$returner = null;
		if ($result->RecordCount() != 0) {
			$returner =& $this->_returnPublisherFromRow($result->GetRowAssoc(false));
		}
		$result->Close();
		unset($result);
		return $returner;
	}

	/**
	 * Retrieve a publisher by url.
	 * @param $url string
	 * @return Publisher
	 */
	function &getPublisherByUrl($url) {
		$returner = null;
		$result =& $this->retrieve(
			'SELECT * FROM publishers WHERE url = ?', $url
		);

		if ($result->RecordCount() != 0) {
			$returner =& $this->_returnPublisherFromRow($result->GetRowAssoc(false));
		}
		$result->Close();
		unset($result);
		return $returner;
	}

	/**
	 * Internal function to return a Publisher object from a row.
	 * @param $row array
	 * @return Publisher
	 */
	function &_returnPublisherFromRow(&$row) {
		$publisher = new Publisher();
		$publisher->setId($row['publisher_id']);
		$publisher->setUrl($row['url']);
		$publisher ->setSequence($row['seq']);
		$publisher ->setEnabled($row['enabled']);
		//$publisher ->setVisible($row['visible']);
                $publisher ->setLogo($row['logo']);
                $publisher ->setTitle($row['title']);
                $publisher ->setQueries13m($row['queries13m']);
                $publisher ->setQueries5y($row['queries5y']);
                $publisher ->setQueries5yt($row['queries5yt']);
                $publisher ->setCountries($row['countries']);
                $publisher ->setCountries10($row['countries10']);
                $publisher ->setTop30($row['top30']);
                $publisher ->setTop30LastYear($row['top30_prev']);




		HookRegistry::call('PublisherDAO::_returnPublisherFromRow', array(&$publisher, &$row));

		return $publisher;
	}

	/**
	 * Insert a new publisher.
	 * @param $publisher Publisher
	 */
	function insertPublisher(&$publisher) {
		$this->update(
			'INSERT INTO publishers
				(title, url, logo, seq, enabled, visible)
				VALUES
				(?, ?, ?, ?, ?, ?)',
			array(
				$publisher->getTitle(),
				$publisher->getUrl(),
				$publisher->getLogo(),
				$publisher->getSequence() == null ? 0 : $publisher->getSequence(),
				$publisher->getEnabled() ? 1 : 0,
                                $publisher->getVisible() ? 1 : 0
			)
		);

		$publisher->setId($this->getInsertPublisherId());
		return $publisher->getId();
	}

	/**
	 * Update an existing publisher.
	 * @param $publisher Publisher
	 */
	function updatePublisher(&$publisher) {
		return $this->update(
			'UPDATE publishers
				SET
					title = ?,
					url = ?,
					logo = ?,
					seq = ?,
					enabled = ?,
					visible = ?,
					queries13m = ?,
                                        queries5y = ?,
                                        countries = ?,
                                        top30 = ?,
                                        top30_prev= ?,
                                        queries5yt = ?,
                                        countries10 = ?

				WHERE publisher_id = ?',
			array(
				$publisher->getTitle(),
				$publisher->getUrl(),
				$publisher->getLogo(),
				$publisher->getSequence(),
				$publisher->getEnabled() ? 1 : 0,
				$publisher->getVisible() ? 1 : 0,
		                $publisher->getQueries13m() ? 1 : 0,
                		$publisher->getQueries5y() ? 1 : 0,
		                $publisher->getQueries5yt() ? 1 : 0,
		                $publisher->getCountries() ? 1 : 0,
                		$publisher->getCountries10() ? 1 : 0,
		                $publisher->getTop30() ? 1 : 0,
                		$publisher->getTop30LastYear() ? 1 : 0,
				$publisher->getId()
			)
		);
	}

	/**
	 * Delete a publisher, INCLUDING ALL DEPENDENT ITEMS.
	 * @param $publisher Publisher
	 */
	function deletePublisher(&$publisher) {
		return $this->deletePublisherById($publisher->getId());
	}

	/**
	 * Delete a publisher by ID, INCLUDING ALL DEPENDENT ITEMS.
	 * @param $publisherId int
	 */
	function deletePublisherById($publisherId) {
               $result =& $this->retrieve(
			'DELETE FROM publishers WHERE publisher_id = ?', $publisherId
		);
	}

	/**
	 * Retrieve all publishers.
	 * @return DAOResultFactory containing matching publishers
	 */
	function &getPublishers($rangeInfo = null) {
		$result =& $this->retrieveRange(
			'SELECT * FROM publishers ORDER BY seq',
			false, $rangeInfo
		);

		$returner = new DAOResultFactory($result, $this, '_returnPublisherFromRow');
		return $returner;
	}

	/**
	 * Retrieve all enabled publishers
	 * @return array Publishers ordered by sequence
	 */
	function &getEnabledPublishers() {
		$result =& $this->retrieve(
			'SELECT * FROM publishers WHERE enabled=1 ORDER BY title'
		);

		$resultFactory = new DAOResultFactory($result, $this, '_returnPublisherFromRow');
		return $resultFactory;
	}

	/**
	 * Retrieve the IDs and titles of all publishers in an associative array.
	 * @return array
	 */
	function &getPublishersTitles() {
		$publishers = array();

		$publisherIterator =& $this->getPublishers();
		while ($publisher =& $publisherIterator->next()) {
			$publishers[$publisher->getId()] = $publisher->getTitle();
			unset($publisher);
		}
		unset($publisherIterator);

		return $publishers;
	}

	/**
	 * Retrieve enabled publishers IDs and titles in an associative array
	 * @return array
	 */
	function &getEnabledPublishersTitles() {
		$publishers = array();

		$publisherIterator =& $this->getEnabledPublishers();
		while ($publisher=& $publisherIterator->next()) {
			$publishers[$publisher->getId()] = $publisher->getTitle();
			unset($publisher);
		}
		unset($publisherIterator);

		return $publishers;
	}

	/**
	 * Check if a publisher exists with a specified url.
	 * @param $url the url of the publisher
	 * @return boolean
	 */
	function publisherExistsByUrl($url) {
		$result =& $this->retrieve(
			'SELECT COUNT(*) FROM publishers WHERE url = ?', $url
		);
		$returner = isset($result->fields[0]) && $result->fields[0] == 1 ? true : false;

		$result->Close();
		unset($result);

		return $returner;
	}

	/**
	 * Sequentially renumber publishers in their sequence order.
	 */
	function resequencePublishers() {
		$result =& $this->retrieve('SELECT publisher_id FROM publishers ORDER BY seq');

		for ($i=1; !$result->EOF; $i++) {
			list($publisherId) = $result->fields;
			$this->update(
				'UPDATE publishers SET seq = ? WHERE publisher_id = ?',
				array(
					$i,
					$publisherId
				)
			);
			$result->moveNext();
		}
		$result->close();
		unset($result);
	}

	/**
	 * Get the ID of the last inserted publisher.
	 * @return int
	 */
	function getInsertPublisherId() {
		return $this->getInsertId('publishers', 'publisher_id');
	}


        function updateSettingPublisher($publisherId,$settingName,$value) {
                $query ="UPDATE publishers SET ".$settingName." = ".$value." WHERE publisher_id=".$publisherId;
                return $this->update($query);
        }

        function &getEnabledPublishersTitlesSuggestion() {
                $publisherIterator =& $this->getEnabledPublishers();
                $publishers='';
                while ($publisher=& $publisherIterator->next()) {
                        $publishers = $publishers ."\"" .  $publisher->getTitle() . "\", ";
                        unset($publisher);
                }
                unset($publisherIterator);

                return $publishers;
        }

        function &getPublisherByTitle($title) {
                $returner = null;
                $result =& $this->retrieve(
                        'SELECT * FROM publishers WHERE LOWER(title) = ? and enabled=1', $title
                );

                if ($result->RecordCount() != 0) {
                        $returner =& $this->_returnPublisherFromRow($result->GetRowAssoc(false));
                }
                $result->Close();
                unset($result);
                return $returner;
        }

        function getPublisherInfo($arg){ 
            $institucio = array();
            $result = &$this->retrieve('SELECT * FROM publishers WHERE publisher_id=?',$arg);
            while (!$result->EOF) {
                $row = &$result->getRowAssoc(false);
                $institucio=array("publisher_id"=>$arg,"institution"=>$row['title'],"url"=>$row['url'],"logo"=>$row['logo']);
                $result->MoveNext();                
            }
            return  $institucio;
        }

        /**Get all Publishers visibles 
         * @author jfarinos
         * 
         * @return type
         */
        function getAllPublishers(){
            $publishers = array();
            $result = &$this->retrieve('SELECT publisher_id,title FROM publishers WHERE enabled=1 ORDER BY publisher_id');                        
            //$participants = array();           
            $sql = "SELECT publisher_id,title FROM publishers WHERE enabled=1 ORDER BY publisher_id"; 
            //$this->assign('publishers', $result->getAssoc($sql)); 
            
            while(!$result->EOF){
                //$publishers = $result->GetRowAssoc(false);
                $idPublishers = $result->fields[0];  
                $title = $result->fields[1];
                $publishers[$idPublishers] = $title;                
                $result->moveNext();
                //$titlePublisher = $result->Fields('title');                
            }                 
            return $publishers;
		}
		
		/**
		 * Get all Publishers by Journal_id
		 *
		 * @param [type] $id
		 * @return void
		 */
		function getMyPublishers($journal_id){
			$publishers = array();
			
			//EXAMPLE: select setting_value from journal_settings where setting_name="publishers" AND journal_id = 158 ;
			$publishersById = &$this->retrieve('SELECT setting_value FROM journal_settings WHERE setting_name="publishers" AND journal_id = ? ;',$journal_id);
			$setting_value = array();

			//get publishers by journal_id serialized
			while(!$publishersById->EOF){
				//$result = &$this->retrieve('SELECT publisher_id,title FROM publishers WHERE enabled=1 ORDER BY publisher_id');
				$setting_value = $publishersById->fields[0]; 
				//print_r($setting_value);                                 
                $publishersById->moveNext();
                
			}                 
			
			//unserialized data
			$setting_valueUnserializable = unserialize($setting_value);
			
			
			$publicadors =array();
			foreach($setting_valueUnserializable as $k=>$v){
				//obtindre les dades de cada publisher_id
				$result = &$this->retrieve('SELECT publisher_id,title,url,logo FROM publishers WHERE enabled=1 AND publisher_id = ? ORDER BY publisher_id;',$v['institution']);  
				while(!$result->EOF){
					//$publisher_id = $result->fields['publisher_id'];  
                	//$publisher_title = $result->fields['title'];
					//$publisher_url = $result->fields['url'];
					//$publisher_logo = $result->fields['logo'];
					$array1 = array(
						'id' => $result->fields['publisher_id'],
						'title' => $result->fields['title'],
						'url' =>  $result->fields['url'],
						'logo' => $result->fields['logo']
					);
					
					array_push($publicadors,$array1);					
                	$result->moveNext();
				}
			}

			
			//print_r($publicadors['logo']);
			//die();

			return $publicadors;

		}		
}

?>