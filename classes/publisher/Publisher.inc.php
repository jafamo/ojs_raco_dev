<?php

/**
 * @defgroup publisher 
 */

/**
 * @file classes/publisher/Publisher.inc.php
 *
 * 2012 CESCA
 *
 * @class Publisher 
 * @ingroup publisher
 * @see PublisherDAO
 *
 * @brief Describes basic publisher properties.
 */


class Publisher extends DataObject {
	/**
	 * Constructor.
	 */
	function Publisher() {
		parent::DataObject();
	}

	/**
	 * Get the title of the publisher.
	 * @return string
	 */
	function getTitle() {
		return $this->getData('title');
	}

        /**
         * Set enabled flag of publisher
         * @param $enabled int
         */
        function setTitle($title) {
                return $this->setData('title',$title);
        }


	/**
	 * Get enabled flag of publisher 
	 * @return int
	 */
	function getEnabled() {
		return $this->getData('enabled');
	}

	/**
	 * Set enabled flag of publisher 
	 * @param $enabled int
	 */
	function setEnabled($enabled) {
		return $this->setData('enabled',$enabled);
	}

        /**
         * Get visible flag of publisher
         * @return int
         */
        function getVisible() {
                return $this->getData('visible');
        }

        /**
         * Set visible flag of publisher
         * @param $visible int
         */
        function setVisible($visible) {
                return $this->setData('visible',$visible);
        }

	/**
	 * Get ID of publisher.
	 * @return int
	 */
	function getPublisherId() {
		if (Config::getVar('debug', 'deprecation_warnings')) trigger_error('Deprecated function.');
		return $this->getId();
	}

	/**
	 * Set ID of publisher.
	 * @param $publisherId int
	 */
	function setPublisherId($publisherId) {
		if (Config::getVar('debug', 'deprecation_warnings')) trigger_error('Deprecated function.');
		return $this->setId($publisherId);
	}

	/**
	 * Get path to publisher (in URL).
	 * @return string
	 */
	function getUrl() {
		return $this->getData('url');
	}

	/**
	 * Set path to publisher (in URL).
	 * @param $path string
	 */
	function setUrl($url) {
		return $this->setData('url', $url);
	}

	/**
	 * Get sequence of publisher in site table of contents.
	 * @return float
	 */
	function getSequence() {
		return $this->getData('sequence');
	}

	/**
	 * Set sequence of publisher in site table of contents.
	 * @param $sequence float
	 */
	function setSequence($sequence) {
		return $this->setData('sequence', $sequence);
	}

        /**
         * Get logo of publisher
         * @return int
         */
        function getLogo() {
                return $this->getData('logo');
        }

        /**
         * Set logo of publisher
         * @param $logo int
         */
        function setLogo($logo) {
                return $this->setData('logo',$logo);
        }

// STATISTICS

        function getQueries13m() {
                return $this->getData('queries13m');
        }

        function setQueries13m($queries13m) {
                return $this->setData('queries13m', $queries13m);
        }

       function getQueries5y() {
                return $this->getData('queries5y');
        }

        function setQueries5y($queries5y) {
                return $this->setData('queries5y', $queries5y);
        }

	function getQueries5yt() {
		return  $this->getData('queries5yt');
	}

        function setQueries5yt($queries5yt) {
                return $this->setData('queries5yt', $queries5yt);
        }


       function getCountries() {
                return $this->getData('countries');
        }

        function setCountries($countries) {
                return $this->setData('countries', $countries);
        }

       function getCountries10() {
                return $this->getData('countries10');
        }

        function setCountries10($countries10) {
                return $this->setData('countries10', $countries10);
        }


       function getTop30() {
                return $this->getData('top30');
        }

        function setTop30($top30) {
                return $this->setData('top30', $top30);
        }

        function getTop30LastYear() {
                return $this->getData('top30next');
        }
        function setTop30LastYear($top30) {
                return $this->setData('top30next', $top30);
        }



}

?>